using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;  //FirstOrDefault

public class KeyBind : MonoBehaviour
{
    public static Dictionary<string, KeyCode> keys = new Dictionary<string, KeyCode>();
    public Text jump, interact; // right, left 제외
    private GameObject currentKey;

    public static KeyBind keybind;

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this);

        if (keybind == null) keybind = this;
        else if (keybind != this)   Destroy(gameObject);

        // keys.Add("Right", KeyCode.RightArrow);
        // keys.Add("Left", KeyCode.LeftArrow);
        if ( !keys.ContainsKey("Jump") ) {  // 최초 한번만 지정된 키 바인딩 하도록
            keys.Add("Jump", KeyCode.Space);
            keys.Add("Interact", KeyCode.Z);
            jump.text = keys["Jump"].ToString();
            interact.text = keys["Interact"].ToString();
        }
        
        // right.text = keys["Right"].ToString();
        // left.text = keys["Left"].ToString();
        
    }
    private void Update()
    {
        
    }
    private void OnGUI()
    {   //currentKey = 클릭한 버튼, e.keyCode = 변경하려고 누른 키(키입력)
        if (currentKey != null)
        {
            Event e = Event.current;    // 발생한 이벤트 (변경할 키 누르는 것일거임)
            if (e.isKey)
            {
                // 클릭한 오브젝트의 이름으로 값을 찾아 변경할것임, 버튼 오브젝트 이름 달라지면 안됨..
                keys[currentKey.name] = e.keyCode;  //키값 변경 
                // UI글씨 현재 누른 키로 변경
                currentKey.transform.GetChild(0).GetComponent<Text>().text = e.keyCode.ToString();
                currentKey = null;  // 변경완료후 null
            }
        }
    }

    public void ChangeKey(GameObject clicked)
    {
        currentKey = clicked;   // currentKey는 클릭된 오브젝트, 클릭하여 curruntKey != null이 되면  OnGUI 안에 if문 실행됨
    }
}
