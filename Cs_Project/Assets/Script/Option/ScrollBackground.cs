using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollBackground : MonoBehaviour
{
    Player player;
    private MeshRenderer render;
    private float offset;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        render = GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (player.groundCheck == false)
        {
            offset += Time.deltaTime * player.speed / 20 * player.direction.x;
            render.material.mainTextureOffset = new Vector2(offset, 0);
        }
    }
}
