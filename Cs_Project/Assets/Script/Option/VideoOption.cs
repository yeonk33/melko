using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class VideoOption : MonoBehaviour
{
    FullScreenMode screenMode;
    public Dropdown resolutionDropdown;
    public Toggle fullscreenBtn;
    List<Resolution>resolutions = new List<Resolution>(); // 해상도 리스트
    int resolutionNum;
    // Start is called before the first frame update
    void Start()
    {
        Application.targetFrameRate = 60;
        InitUI();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void InitUI()
    {
        for(int i = 0 ; i < Screen.resolutions.Length; i++)
        {
            if(!(Screen.resolutions[i].refreshRate == 60 || Screen.resolutions[i].refreshRate == 144))
                continue;
            if(Screen.resolutions[i].width == 1280 && Screen.resolutions[i].height == 720)
                resolutions.Add(Screen.resolutions[i]);
            else if(Screen.resolutions[i].width == 1440 && Screen.resolutions[i].height == 810)
                resolutions.Add(Screen.resolutions[i]);
            else if(Screen.resolutions[i].width == 1920 && Screen.resolutions[i].height == 1080)
                resolutions.Add(Screen.resolutions[i]);
            else if(Screen.resolutions[i].width == 2560 && Screen.resolutions[i].height == 1440)
                resolutions.Add(Screen.resolutions[i]);
        }

        resolutionDropdown.options.Clear();
        int optionNum = 0;

        foreach(Resolution item in resolutions)
        {
            Dropdown.OptionData option = new Dropdown.OptionData();
            option.text = item.width + "x" + item.height + " " + item.refreshRate + "hz";
            resolutionDropdown.options.Add(option);

            if(item.width == Screen.width && item.height == Screen.height)
                resolutionDropdown.value = optionNum;
            optionNum++;
        }
        resolutionDropdown.RefreshShownValue();

        fullscreenBtn.isOn = Screen.fullScreenMode.Equals(FullScreenMode.FullScreenWindow)?true:false;
    }

    public void FullScreenBtn(bool isFull)
    {
        
        screenMode = isFull ? FullScreenMode.FullScreenWindow : FullScreenMode.Windowed;
    }

    public void DropboxOptionChange(int x)
    {
        resolutionNum = x;
    }

    public void OkBtnClick()
    {
        Screen.SetResolution(resolutions[resolutionNum].width, resolutions[resolutionNum].height, screenMode);
    }
}
