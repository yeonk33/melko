using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Title : MonoBehaviour
{
    public static List<int> stage1_randomList;
    public static List<int> stage2_randomList;
    public static List<int> stage3_randomList;

    public string sceneName = "PlayerTestMap";
    public int scene_number_one;
    public int scene_number_two;

    void Start()
    {
        Stage1_RestList();
        Stage2_RestList();
        Stage3_RestList();
    }

    public void ClickStart() // ���ӽ���
    {
        Debug.Log("���ӽ���");
        SceneManager.LoadScene(sceneName);
    }

    public void ClickLoad() // ���� �ε�
    {
        Debug.Log("���ӷε�");

    }

    public void ClickSet() // ȯ�漳��
    {
        Debug.Log("ȯ�漳��");

    }

    public void ClickExit() // ���� ������
    {
        Debug.Log("��������");
        Application.Quit();
    }

    public void Stage1_RestList()
    {
        stage1_randomList = new List<int>(){3,4}; // ������ �� ����
    }

    public void Stage2_RestList()
    {
        stage2_randomList = new List<int>(){9,10}; // ������ �� ����
    }

    public void Stage3_RestList()
    {
        stage3_randomList = new List<int>(){13,14,15}; // ������ �� ����
    }

    public static void stage1_MixScene() // �� ����
    {
        List<int> list = new List<int>();
        int count = stage1_randomList.Count;
        for(int i = 0; i < count; i++)
        {
            int rand = Random.Range(0, stage1_randomList.Count);
            list.Add(stage1_randomList[rand]);
            stage1_randomList.RemoveAt(rand);
        }
        stage1_randomList = list;
    }

    public static void stage2_MixScene() // �� ����
    {
        List<int> list = new List<int>();
        int count = stage2_randomList.Count;
        for(int i = 0; i < count; i++)
        {
            int rand = Random.Range(0, stage2_randomList.Count);
            list.Add(stage2_randomList[rand]);
            stage2_randomList.RemoveAt(rand);
        }
        stage2_randomList = list;
    }

    public static void stage3_MixScene() // �� ����
    {
        List<int> list = new List<int>();
        int count = stage3_randomList.Count;
        for(int i = 0; i < count; i++)
        {
            int rand = Random.Range(0, stage3_randomList.Count);
            list.Add(stage3_randomList[rand]);
            stage3_randomList.RemoveAt(rand);
        }
        stage3_randomList = list;
    }
}
