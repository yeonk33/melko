using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadManager : MonoBehaviour
{
    public string scene;
    public int end;
    public float health;
    public string save_Scene_Name;
    public int save_end = 0;
    public float save_hp = 5;

    private bool end_chk = false;
    Player player;

    public static LoadManager Instance;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        DontDestroyOnLoad(transform.gameObject);
        if (Instance == null) Instance = this;
        if (Instance != this) Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GameEnd()
    {
        PlayerPrefs.SetInt("End?", save_end);
        PlayerPrefs.SetString("SceneName", save_Scene_Name);
        PlayerPrefs.SetFloat("MyHp", save_hp);
    }

    public void GameSave()
    {
        PlayerPrefs.SetString("SceneName", save_Scene_Name);
        PlayerPrefs.SetFloat("MyHp", save_hp);

        Debug.Log(PlayerPrefs.GetString("SceneName"));
        player.Hp = health;
    }

    public void GameLoad()
    {
        if(!PlayerPrefs.HasKey("MyHp"))
        {
            Debug.Log("Ű������.");
            return;
        }
        
        scene = PlayerPrefs.GetString("SceneName");
        end = PlayerPrefs.GetInt("End?");
        health = PlayerPrefs.GetFloat("MyHp");
        player.Hp = health;
        Debug.Log(scene);
        
        if(end == 1 && !end_chk)
        {
            save_end = 1;
            end_chk = true;
        }

        SceneManager.LoadScene(scene);
    }

    public void SaveDelete()
    {
        PlayerPrefs.DeleteAll();
    }
}
