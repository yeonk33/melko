using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BgmChange : MonoBehaviour
{
    Player player;

    // Inspector 영역에 표시할 배경음악 이름
    public string bgmName = "";

    public GameObject SoundManager;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        SoundManager = GameObject.FindGameObjectWithTag("BgmManager");
        Debug.Log("Find");
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            SoundManager.GetComponent<PlayMusicOperator>().PlayBGM(bgmName);
            Debug.Log("enter");
        }
    }
}