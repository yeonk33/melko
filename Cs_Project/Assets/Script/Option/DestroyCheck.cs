using UnityEngine;

public class DestroyCheck : MonoBehaviour
{
    public static DestroyCheck Instance;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
    }
}