using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public PauseManager pm;
    Scene scene;
    GameObject canvas;

    public void SettingMenu(){  // 환경설정창 열음
        pm.settings = true;
    }

    public void SettingMenuExit(){  // 환경설정창 닫음        
        pm.settings = false;
        canvas = GameObject.Find("Canvas/Panel");
        scene = SceneManager.GetActiveScene(); // 현재 씬 이름 가져오기

        if ( (scene.name == "GameTitle") || (scene.name == "GameTitle_second")) {
            canvas.transform.GetChild(5).gameObject.SetActive(false); // 일시정지창 비활성화
            canvas.transform.GetChild(6).gameObject.SetActive(false); // 세팅버튼 비활성화
            canvas.transform.GetChild(7).gameObject.SetActive(false); // 타이틀로 비활성화
            canvas.transform.GetChild(8).gameObject.SetActive(false); // 게임종료 비활성화
        }
        else if ( scene.name != "GameTitle" ) {
            canvas.transform.GetChild(5).gameObject.SetActive(true); // 일시정지창 활성화
            canvas.transform.GetChild(6).gameObject.SetActive(true); // 세팅버튼 활성화
            canvas.transform.GetChild(7).gameObject.SetActive(true); // 타이틀로 활성화
            canvas.transform.GetChild(8).gameObject.SetActive(true); // 게임종료 활성화
        }
    }

    public void PauseMenuExit(){
        canvas = GameObject.Find("Canvas/Panel");
        pm.isPaused = false;
        Time.timeScale = 1f;
        canvas.transform.GetChild(5).gameObject.SetActive(false); // 일시정지창 비활성화
        canvas.transform.GetChild(6).gameObject.SetActive(false); // 세팅버튼 비활성화
        canvas.transform.GetChild(7).gameObject.SetActive(false); // 타이틀로 비활성화
        canvas.transform.GetChild(8).gameObject.SetActive(false); // 게임종료 비활성화
    }

    public void goTitle(){  // 타이틀 화면으로
        Time.timeScale = 1f;
        SceneManager.LoadScene("GameTitle");
        canvas = GameObject.Find("Canvas/Panel");
        canvas.transform.GetChild(5).gameObject.SetActive(false); // 일시정지창 비활성화
        canvas.transform.GetChild(6).gameObject.SetActive(false); // 세팅버튼 비활성화
        canvas.transform.GetChild(7).gameObject.SetActive(false); // 타이틀로 비활성화
        canvas.transform.GetChild(8).gameObject.SetActive(false); // 게임종료 비활성화
    }

    public void QuitGame(){ // 게임종료
        Application.Quit();
    }
    
}
