using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseManager : MonoBehaviour
{
    public bool isPaused;
    public bool settings;
    public bool gameTitle;
    public GameObject pauseMenu;
    public static PauseManager Instance = null;

    void Awake() {
        if (Instance == null) Instance = this;
        if (Instance != this) Destroy(gameObject);  // 중복 방지
        DontDestroyOnLoad(this);
    }

    
    // Start is called before the first frame update
    void Start()
    {
        pauseMenu = GameObject.Find("Canvas/Panel");
        isPaused = false;
        settings = false;
        gameTitle = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape)){
            if (!settings && !gameTitle){
                if(isPaused){
                    Resume();
                }
                else{
                    Pause();
                }
            }
        }
    }

    public void Resume(){   // 게임재게
        pauseMenu.transform.GetChild(5).gameObject.SetActive(false); // 일시정지창 비활성화
        pauseMenu.transform.GetChild(6).gameObject.SetActive(false); // 세팅버튼 비활성화
        pauseMenu.transform.GetChild(7).gameObject.SetActive(false); // 타이틀로 비활성화
        pauseMenu.transform.GetChild(8).gameObject.SetActive(false); // 게임종료 비활성화

        isPaused = false;
        Time.timeScale = 1f;
    }
    
    public void Pause(){    // 게임정지
        pauseMenu.transform.GetChild(5).gameObject.SetActive(true); // 일시정지창 비활성화
        pauseMenu.transform.GetChild(6).gameObject.SetActive(true); // 세팅버튼 비활성화
        pauseMenu.transform.GetChild(7).gameObject.SetActive(true); // 타이틀로 비활성화
        pauseMenu.transform.GetChild(8).gameObject.SetActive(true); // 게임종료 비활성화

        isPaused = true;
        Time.timeScale = 0f;
    }
    
    void OnEnable() { 
        SceneManager.sceneLoaded += OnSceneLoaded;
    }
    
    void OnSceneLoaded(Scene scene, LoadSceneMode mode) {   // 씬 전환시 실행됨
        if ( scene.name == "GameTitle" ) {
            gameTitle = true;
        }
        else if ( scene.name != "GameTitle" ) {
            gameTitle = false;
        }

    }

}
