using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyThis : MonoBehaviour
{
    // public static DontDestroyThis dont;

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this);
        // if (dont == null) dont = this;
        // else if (dont != this)   Destroy(gameObject);
    }
}
