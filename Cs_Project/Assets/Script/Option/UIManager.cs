using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    Scene scene;
    public GameObject canvas;   
    public GameObject graphic;   
    public static UIManager Instance;

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this);
        if (Instance == null) Instance = this;
        if (Instance != this) Destroy(gameObject);  // 중복 방지

        canvas = GameObject.Find("Canvas/Panel");
        graphic = GameObject.Find("Canvas/Panel/환경설정창");
        Title();
    }

    void OnEnable() { 
        SceneManager.sceneLoaded += OnSceneLoaded;
    }
    
    void OnSceneLoaded(Scene scene, LoadSceneMode mode) {   // 씬 전환시 실행됨
        if ( (scene.name == "GameTitle") || (scene.name == "GameTitle_second") ) {
            Title();
        }
        else if ( scene.name != "GameTitle" ) {
            InGame();
        }

        if (scene.name == "GameTitle_second") { // 2회차 타이틀에서는 start버튼 비활성화
            canvas.transform.GetChild(1).gameObject.SetActive(false);  // 1: START 버튼
        }
    }

    // Update is called once per frame
    void Update()
    {
        //scene = SceneManager.GetActiveScene();
        
    }

    void Title() {
        canvas.transform.GetChild(0).gameObject.SetActive(true);  // 0: 메인화면 그림
        canvas.transform.GetChild(1).gameObject.SetActive(true);  // 1: START 버튼
        canvas.transform.GetChild(2).gameObject.SetActive(true);  // 2: SETTING 버튼
        canvas.transform.GetChild(3).gameObject.SetActive(true);  // 3: LOAD 버튼
        canvas.transform.GetChild(4).gameObject.SetActive(true);  // 4: QUIT 버튼
        graphic.transform.GetChild(3).gameObject.SetActive(true);  // 해상도 버튼

        canvas.transform.GetChild(13).gameObject.SetActive(false);  // 13: heart아이콘
        canvas.transform.GetChild(14).gameObject.SetActive(false);  // 14: hp 체력
    }

    void InGame() {
        canvas.transform.GetChild(0).gameObject.SetActive(false);  // 0: 메인화면 그림
        canvas.transform.GetChild(1).gameObject.SetActive(false);  // 1: START 버튼
        canvas.transform.GetChild(2).gameObject.SetActive(false);  // 2: SETTING 버튼
        canvas.transform.GetChild(3).gameObject.SetActive(false);  // 3: LOAD 버튼
        canvas.transform.GetChild(4).gameObject.SetActive(false);  // 4: QUIT 버튼
        graphic.transform.GetChild(3).gameObject.SetActive(false);  // 해상도 버튼

        canvas.transform.GetChild(13).gameObject.SetActive(true);  // 13: heart아이콘
        canvas.transform.GetChild(14).gameObject.SetActive(true);  // 14: hp 체력
    }
}
