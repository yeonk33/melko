using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class SoundOption : MonoBehaviour
{
    public AudioMixer masterMixer;

    public Slider masterSlider;
    public Slider bgmSlider;
    public Slider sfxSlider;
    public Slider uiSlider;

    //public AudioSource backgroundMusic;
    public AudioSource interfaceSound;

    public static SoundOption Instance;

    void Start()
    {
        DontDestroyOnLoad(transform.gameObject);    // 씬 넘어가도 오브젝트가 삭제되지 않음
        if (Instance == null) Instance = this;
        if (Instance != this) Destroy(gameObject);
    }

    /*
    public void SetMusicVolume(float volume)
    {
        backgroundMusic.volume = volume;
    }

    public void SetUIVolume(float volume)
    {
        interfaceSound.volume = volume;
    }
    */
    
    public void OnSfx()
    {
        interfaceSound.Play();          // 클릭 시 효과음 발생
    }

    public void BgmControl()
    {
        float bgmSound = bgmSlider.value;

        masterMixer.SetFloat("BGM", Mathf.Log10(bgmSound) * 20);
    }

    public void SfxControl()
    {
        float sfxSound = sfxSlider.value;

        masterMixer.SetFloat("SFX", Mathf.Log10(sfxSound) * 20);
    }

    public void UIControl()
    {
        float uiSound = uiSlider.value;

        masterMixer.SetFloat("UI", Mathf.Log10(uiSound) * 20);
    }

    public void MasterControl()
    {
        float masterSound = masterSlider.value;

        masterMixer.SetFloat("Master", Mathf.Log10(masterSound) * 20);
    }

    public void ToggleAudioVolume()
    {
        AudioListener.volume = AudioListener.volume == 0 ? 1 : 0;    // 음소거 토글       
        // Audio 출력을 바꾸는 것이 아닌 Audio 입력받는 쪽(AudioListener)의 볼륨을 줄이는 것
    }
}
