using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoollingItem : MonoBehaviour
{
    SpriteRenderer spriteRenderer;
    CapsuleCollider2D capsuleCollider2D;
    Player player;
    [SerializeField] private float currTime = 5; // 아이템 재생성 시간 설정
    private bool activeCoollingItem = true; //아이템 활성상태
    private float itemTime; // 흐른시간 저장 변수

    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        capsuleCollider2D = GetComponent<CapsuleCollider2D>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        itemTime = 0;
        //this.gameObject.SetActive(false); // 점프 아이템 기본 비활성화
    }
    // Update is called once per frame
    void Update()
    {
        itemTime += Time.deltaTime;
        if(!activeCoollingItem) // 아이템이 비활성화 되어있으면
        {
            if(itemTime > currTime) // 재생성시간이 흘렀다면
            {
                capsuleCollider2D.enabled = true; // 콜라이더 활성화
                spriteRenderer.enabled = true; // 렌더러 활성화
                activeCoollingItem = true; // 아이템 활성화
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player") && activeCoollingItem) // 플레이어이고, 아이템이 활성화되어있으면
        {
            if(player.playerfired > 50) // 과열상태가 10 보다 크면
            {
                player.playerfired -= 50; // 과열 10 회복
            }else{
                player.playerfired -= 1; // 그렇지 않으면 1 회복
            }

            itemTime = 0;
            capsuleCollider2D.enabled = false; // 콜라이더 비활성화
            spriteRenderer.enabled = false; // 렌더러 비활성화
            activeCoollingItem = false; // 아이템 비활성화
        }
    }
}
