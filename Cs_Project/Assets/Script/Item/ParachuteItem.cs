using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParachuteItem : MonoBehaviour
{
    SpriteRenderer spriteRenderer;
    Player player;
    [SerializeField] private float currTime = 5; // 아이템 재생성 시간 설정
    private bool activeParachuteItem = true; //아이템 활성상태
    public bool activeParachute = false;
    private float itemTime; // 흐른시간 저장 변수
    public GameObject parachute;


    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        itemTime = 0;
    }

    void Update()
    {
        itemTime += Time.deltaTime;
        if (!activeParachuteItem) // 아이템이 비활성화 되어있으면
        {
            if (itemTime > currTime) // 재생성시간이 흘렀다면
            {
                spriteRenderer.enabled = true; // 렌더러 활성화
                activeParachuteItem = true; // 아이템 활성화
            }
        }
    }

    void ParachuteActive()
    {
        parachute.gameObject.SetActive(true);// 낙하산 활성화   
        parachute.gameObject.GetComponent<Parachute>().enabled = true;
    }

    public void ParachuteDisabled()
    {
        parachute.gameObject.SetActive(false);// 낙하산 비활성화
        parachute.gameObject.GetComponent<Parachute>().enabled = false;
        parachute.gameObject.GetComponent<Parachute>().space = false;
        player.GetComponent<Rigidbody2D>().drag = 0;
        activeParachute = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && activeParachuteItem) // 플레이어이고, 아이템이 활성화되어있으면
        {
            ParachuteActive();
            /*if(activeParachute == true && player.isGround == true)
            {
                ParachuteDisabled();
            }*/
            itemTime = 0;
            spriteRenderer.enabled = false; // 렌더러 비활성화
            activeParachuteItem = false; // 아이템 비활성화
        }
    }
}

