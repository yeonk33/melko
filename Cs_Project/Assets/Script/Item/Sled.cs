using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sled : MonoBehaviour
{
    Player player;
    private float cooltime;
    private bool activeSled = true;

    void OnEnable()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        SledActive();
        CancelInvoke();
        activeSled = true;
        cooltime = 0;
    }

    void Start()
    {
        //player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    void SledActive()
    {
        this.gameObject.SetActive(true);// 썰매 활성화   
        player.speed = 13.0f;
        player.jumpPower = 12.0f;
        player.gameObject.GetComponent<Stage2_Frozen>().enabled = false;
    }

    void SledDisabled()
    {
        this.gameObject.SetActive(false);// 썰매 비활성화
        player.speed = 3.0f;
        player.jumpPower = 10.0f;
        this.gameObject.GetComponent<Sled>().enabled = false;
        player.gameObject.GetComponent<Stage2_Frozen>().enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        player.isJumping = false;
        Debug.Log(player.isJumping);
        cooltime += Time.deltaTime;
        if (activeSled)
        {
            if (Input.GetKey(KeyBind.keys["Interact"])) // 해당키 입력시
            {
                //Invoke("SledDisabled", 3f); // 3초뒤 비활성화
                SledDisabled();
                activeSled = false;
            }
            else if (cooltime > 5) //z키 입력시 비활성화
            {
                SledDisabled();
                activeSled = false;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag=="SledItem" && activeSled) // 플레이어이고, 아이템이 활성화되어있으면
        {
            cooltime = 0;
        }

        if (collision.tag == "Fallen" && activeSled) //낙하산 활성 및 플레이어가 땅에 닿으면 
        {
            SledDisabled();
            activeSled = false;
        }
    }
}
