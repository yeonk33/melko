using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezingItem : MonoBehaviour
{
    SpriteRenderer spriteRenderer;
    Player player;
    [SerializeField] private float currTime = 5; // 아이템 재생성 시간 설정
    private bool activeFreezingItem = true; //아이템 활성상태
    private float itemTime; // 흐른시간 저장 변수
    public GameObject FreezingGun;

    /*
    public GameObject FreezingBlock; // 얼려진 블록
    public GameObject LavaBlock; // 용암블록
    */

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        itemTime = 0;
    }

    void Update()
    {
        itemTime += Time.deltaTime;
        if (!activeFreezingItem) // 아이템이 비활성화 되어있으면
        {
            if (itemTime > currTime) // 재생성시간이 흘렀다면
            {
                spriteRenderer.enabled = true; // 렌더러 활성화
                activeFreezingItem = true; // 아이템 활성화
            }
        }
    }

    /*void BlockActive()
    {
        FreezingBlock.gameObject.SetActive(true);// 얼려진 블록 활성화
        LavaBlock.gameObject.SetActive(false);// 용암 블록 비활성화
    }

    void BlockDisabled()
    {
        FreezingBlock.gameObject.SetActive(false); // 얼려진 블록 비활성화   
        LavaBlock.gameObject.SetActive(true); // 용암 블록 활성화
    }
    */

    void GunActive()
    {
        FreezingGun.gameObject.SetActive(true);// 총 활성화
        FreezingGun.gameObject.GetComponent<Shooting>().enabled = true;
    }

    public void GunDisabled()
    {
        FreezingGun.gameObject.SetActive(false);// 총 비활성화
        FreezingGun.gameObject.GetComponent<Shooting>().enabled = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && activeFreezingItem) // 플레이어이고, 아이템이 활성화되어있으면
        {
            GunActive();
            itemTime = 0;
            spriteRenderer.enabled = false; // 렌더러 비활성화
            activeFreezingItem = false; // 아이템 비활성화
        }
    }
}
