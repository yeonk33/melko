using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SledItem : MonoBehaviour
{
    SpriteRenderer spriteRenderer;
    Player player;
    [SerializeField] private float currTime = 5; // 아이템 재생성 시간 설정
    private bool activeSledItem = true; //아이템 활성상태
    private float itemTime; // 흐른시간 저장 변수
    public GameObject Sled;


    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        itemTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
        itemTime += Time.deltaTime;
        if (!activeSledItem) // 아이템이 비활성화 되어있으면
        {
            if (itemTime > currTime) // 재생성시간이 흘렀다면
            {
                spriteRenderer.enabled = true; // 렌더러 활성화
                activeSledItem = true; // 아이템 활성화
            }
        }
    }

    void SledActive()
    {
        Sled.gameObject.SetActive(true);// 썰매 활성화   
        Sled.gameObject.GetComponent<Sled>().enabled = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && activeSledItem) // 플레이어이고, 아이템이 활성화되어있으면
        {
            SledActive();
            itemTime = 0;
            spriteRenderer.enabled = false; // 렌더러 비활성화
            activeSledItem = false; // 아이템 비활성화
        }
    }
}


