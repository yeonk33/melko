using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

public class CinematicItem : MonoBehaviour
{
    public RawImage screen;
    public VideoPlayer videoPlayer = null;
    //public GameObject skipBtn;

    private double time;
    private double curTime;


    private void Start()
    {
        screen.enabled = false;         // 스크린 비활성화
        //skipBtn.SetActive(false);        // 버튼 비활성화
        time = videoPlayer.clip.length; // 재생할 영상 길이
    }

    private void FixedUpdate()
    {
        curTime = videoPlayer.time; // 현재 비디오 재생 시간
        if (time - curTime < 0.1)   // 끝까지 재생해도 현재 비디오 재생시간이 작음..
        {
            StopVideo();    // 비디오 정지
        }
    }

    /*
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            StartCoroutine(StartVideo());   // 비디오 실행
        }
    }
    */

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player" && this.transform.position.x >= 230)
        {
            StartCoroutine(StartVideo());   // 비디오 실행
        }
    }

    IEnumerator StartVideo()
    {
        screen.enabled = true;  // 스크린 다시 활성화
        //skipBtn.SetActive(true);
        videoPlayer.Prepare();  // 비디오 준비
        while (!videoPlayer.isPrepared) // 비디오 준비 기다림
        {
            yield return new WaitForSeconds(0.5f);
        }

        screen.texture = videoPlayer.texture;   // 출력 texture를 RawImage의 texture로 설정

        videoPlayer.Play(); // 재생
    }

    public void StopVideo()
    {
        videoPlayer.Stop(); // 비디오 종료
        //skipBtn.SetActive(false);   // 버튼 비활성화
    }
}
