using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParachutePos : MonoBehaviour
{
    public Vector3 direction;
    public Transform parachutepos;

    void Start()
    {
        parachutepos = GameObject.FindGameObjectWithTag("parachutepos").transform;
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = parachutepos.position;   //gunpos��ġ�� �ѻ���\
        direction.x = Input.GetAxisRaw("Horizontal"); // �¿� �Է¹ޱ�, ��=-1 ��=1

        if (direction.x < 0) // �����̵�
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
        else if (direction.x > 0)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
    }
}
