using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parachute : MonoBehaviour
{
    //public KeyCode button;
    //public GameObject parachute;
    public GameObject player;
    ParachuteItem parachuteitem;
    Player player1;
    public bool space = false; // 공중상태
    private float parachuteTime= 0;
    private float parachuteExit= 0.35f; // 낙하산 해제 키 활성화

    void Awake()
    {
        player1 = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        parachuteitem = GameObject.FindGameObjectWithTag("ParachuteItem").GetComponent<ParachuteItem>();
        parachuteTime = 0;
    }

    void Update()
    {
        //parachute.GetComponent<Rigidbody2D>().drag = 12;
        if (parachuteitem.activeParachute == false && space == false) //낙하산이 펴지기 전일때
        {
            if (Input.GetKeyDown(KeyBind.keys["Interact"]))
            {
                player.GetComponent<Rigidbody2D>().drag = 12;
                parachuteitem.activeParachute = true;
                space = true;
                player1.isJumping = false;
            }
        }

        else if(parachuteitem.activeParachute == true && space == true) //낙하산 사용 후 공중일때
        {
            parachuteTime += Time.deltaTime;
            if (parachuteTime > parachuteExit && Input.GetKeyDown(KeyBind.keys["Interact"])) // 해제키 활성화
            {
                parachuteitem.ParachuteDisabled();
            }
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Ground" && parachuteitem.activeParachute == true) //낙하산 활성 및 플레이어가 땅에 닿으면 
        {
            parachuteitem.ParachuteDisabled(); //낙하산 비활성화
            parachuteitem.activeParachute = false;
        }

        if (collision.tag == "Fallen" && parachuteitem.activeParachute == true) //낙하산 활성 및 플레이어가 땅에 닿으면 
        {
            parachuteitem.ParachuteDisabled(); //낙하산 비활성화
            parachuteitem.activeParachute = false;
        }
    }
    
}
