using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest_Mine_Take : MonoBehaviour
{
    Player player;
    HiddenDoor hiddenDoor;

    public GameObject hudInteract;
    public Transform hudPos;
    private bool QuestClear = false;

    private int takemine = 0;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        hiddenDoor = GameObject.FindGameObjectWithTag("HiddenDoor").GetComponent<HiddenDoor>();
    }

    // Update is called once per frame
    void Update()
    {
        if(QuestClear && hiddenDoor.door.activeSelf == false)
        {
            player.playSound("Open_Hidden");
            hiddenDoor.door.SetActive(true);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player") && Input.GetKey(KeyBind.keys["Interact"]) && player.GetMine) // ÇÃ·¹??Ì¾î??Ì°í, ZÅ°¸¦ ´©¸£°í, ±¤¹°??» È¹µæÇÑ »óÅÂ¸é
        {
            takemine += 1;
            GameObject hudText = Instantiate(hudInteract);
            hudText.transform.position = hudPos.position;
            
            if(takemine < 1)
            {
                hudText.GetComponent<Mine_Text>().interact = "�� ����" + takemine.ToString() + "��";
            }
            else
            {
                QuestClear = true;
                hudText.GetComponent<Mine_Text>().interact = "������ ���� ���� �� ����..!!";
            }
            
            Debug.Log("���� " + takemine + "���� ȹ���Ͽ����ϴ�.");
            player.GetMine = false;
        }
    }
}
