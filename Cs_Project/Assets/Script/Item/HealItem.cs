using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealItem : MonoBehaviour
{
    SpriteRenderer spriteRenderer;
    Player player;
    [SerializeField] private float currTime = 3; // 점프 재생성 시간 설정
    private bool activeHealItem = true; //아이템 활성상태
    private float itemTime; // 흐른시간 저장 변수

    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        itemTime = 0;
        //this.gameObject.SetActive(false); // 점프 아이템 기본 비활성화
    }
    // Update is called once per frame
    void Update()
    {/* 한번만
        itemTime += Time.deltaTime;
        if(!activeHealItem) // 아이템이 비활성화 되어있으면
        {
            if(itemTime > currTime) // 재생성시간이 흘렀다면
            {
                spriteRenderer.enabled = true; // 렌더러 활성화
                activeHealItem = true; // 아이템 활성화
            }
        }
        */
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player") && activeHealItem) // 플레이어이고, 아이템이 활성화되어있으면
        {
            if(player.Hp < 5) // 체력이 5보다 작으면
            {
                player.Hp += 1; // 체력 1 회복
                
                if(player.Hp > 4.5)
                {
                    player.Hp = 5;
                }
            }
            itemTime = 0;
            spriteRenderer.enabled = false; // 렌더러 비활성화
            activeHealItem = false; // 아이템 비활성화
            Destroy(gameObject);
        }
    }
}