using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest_Mine_Give : MonoBehaviour
{
    Player player;

    public GameObject hudInteract;
    public Transform hudPos;

    private float time = 0;
    private int count = 0;
    private bool click = false;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        if(click && !player.GetMine)
        {
            player.animator.SetBool("mine", true);
            time += Time.deltaTime;
        }
        else
            player.animator.SetBool("mine", false);
        
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player") && Input.GetKeyDown(KeyBind.keys["Interact"])) // 플레이어이고, Z키를 누르면
        {
            click = true;
            Debug.Log("광물 캐는 중.....");
            
        }

        if(collision.gameObject.CompareTag("Player") && Input.GetKeyUp(KeyBind.keys["Interact"]) && !player.GetMine)
        {
            GameObject hudText = Instantiate(hudInteract);
            hudText.transform.position = hudPos.position;
            hudText.GetComponent<Mine_Text>().interact = "조금만 더 오래 캤으면 성공했을텐데..";
            Debug.Log("상호작용 실패.");
            click = false;
            time = 0;
        }

        if(time > 2 && !player.GetMine) // 2초동안 키를 누르고 있으면
        {
            GameObject hudText = Instantiate(hudInteract);
            hudText.transform.position = hudPos.position;
            hudText.GetComponent<Mine_Text>().interact = "광물 획득! 얼른 돌아가자";
            Debug.Log("획득하였습니다.");
            player.GetMine = true;
            click = false;
            time = 0;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player")) // 플레이어가 벗어나면
        {
            click = false;
            Debug.Log("범위를 벗어났습니다.");
            time = 0;
        }
    }
}
