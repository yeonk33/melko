using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunPos : MonoBehaviour
{
    public Vector3 direction;
    public Transform gunpos;
    // public Transform Gun;
    void Start()
    {
        gunpos = GameObject.FindGameObjectWithTag("gunpos").transform;
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = gunpos.position;   //gunpos��ġ�� �ѻ���
        direction.x = Input.GetAxisRaw("Horizontal"); // �¿� �Է¹ޱ�, ��=-1 ��=1

        if (direction.x < 0) // �����̵�
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
        else if (direction.x > 0)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
    }
}
