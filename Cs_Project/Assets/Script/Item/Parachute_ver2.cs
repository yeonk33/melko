using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parachute_ver2 : MonoBehaviour
{
    public GameObject player;
    ParachuteItem_ver2 parachuteitem_ver2;
    Player player2;

    // Start is called before the first frame update
    void Awake()
    {
        player2 = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        if (player2.isGround == false)
        {
            //gameObject.SetActive(true);// ���ϻ� Ȱ��ȭ   
            parachuteitem_ver2.ParachuteActive();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Fallen" && parachuteitem_ver2.activeParachute_ver2 == true) //���ϻ� Ȱ�� �� �÷��̾ ���� ������ 
        {
            parachuteitem_ver2.ParachuteDisabled(); //���ϻ� ��Ȱ��ȭ
            parachuteitem_ver2.activeParachute_ver2 = false;
        }
    }
}
