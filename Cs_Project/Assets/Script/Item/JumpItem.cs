using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpItem : MonoBehaviour
{
    SpriteRenderer spriteRenderer;
    CapsuleCollider2D capsuleCollider2D;
    Player player;
    [SerializeField] private float currTime = 3; // 점프 재생성 시간 설정
    private bool activeJumpItem = true; //아이템 활성상태
    private float itemTime; // 흐른시간 저장 변수

    // Start is called before the first frame update
    void Start()
    {
        capsuleCollider2D = GetComponent<CapsuleCollider2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        itemTime = 0;
        //this.gameObject.SetActive(false); // 점프 아이템 기본 비활성화
    }
    // Update is called once per frame
    void Update()
    {
        itemTime += Time.deltaTime;
        if(!activeJumpItem) // 아이템이 비활성화 되어있으면
        {
            if(itemTime > currTime) // 재생성시간이 흘렀다면
            {
                capsuleCollider2D.enabled = true;
                spriteRenderer.enabled = true; // 렌더러 활성화
                activeJumpItem = true; // 아이템 활성화
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player") && activeJumpItem) // 플레이어이고, 아이템이 활성화되어있으면
        {
            player.isGround = true; // isGround 참으로 초기화
            itemTime = 0;
            capsuleCollider2D.enabled = false;
            spriteRenderer.enabled = false; // 렌더러 비활성화
            activeJumpItem = false; // 아이템 비활성화
        }
    }
}