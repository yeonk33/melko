using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public Vector3 direction;
    public SpriteRenderer spriteRenderer;
    public Rigidbody2D rigid2D;

    [SerializeField] public float speed = 6.0f;
    [SerializeField] public float jumpPower = 5.0f;
    [SerializeField] public bool isGround = true;
    [SerializeField] public bool inLadder = false;
    [SerializeField] public bool isJumping = false;
    [SerializeField] public float Hp = 5;
    [SerializeField] public int collisionForce = 3;
    [SerializeField] public float invincibleTime = 1.5f;
    [SerializeField] public int playerfired = 0;
    [SerializeField] public bool GetMine = false;
    [SerializeField] public bool Clear = false;
    [SerializeField] public bool mine = false;
    [SerializeField] public bool fallen = false;
    [SerializeField] private Text myHp;

    public Transform spawnPoint;

    public AudioClip audioJump;
    public AudioClip audioDamaged;
    public AudioClip audioItem;
    public AudioClip audioJumpItem;
    public AudioClip audioCheckPoint;
    public AudioClip audioFallen;
    public AudioClip audioDie;
    public AudioClip audioClear;
    public AudioClip audioFinish;
    public AudioClip audioHidden;
    AudioSource audioSource;

    WindEffect windeffect;
    public Animator animator;
    public Transform transform;
    ParachuteItem parachuteitem;
    ParachuteItem_ver2 parachuteitem2;
  
    LoadManager savePlayer;

    Vector2 rayRight;
    Vector2 rayLeft;
    public bool groundCheck = false;

    void Awake()
    {
        this.audioSource = GetComponent<AudioSource>();
    }

    void Start()
    {
        savePlayer = GameObject.Find("LoadManager").GetComponent<LoadManager>();
        
        // Debug.Log(savePlayer.health);

        if(!PlayerPrefs.HasKey("MyHp"))
            Hp = 5;
        else
            Hp = PlayerPrefs.GetFloat("MyHp");

        myHp = GameObject.Find("Canvas/Panel/HP").GetComponent<Text>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        rigid2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        transform = GetComponent<Transform>();
        rayRight = new Vector2(1, -1);
        rayLeft = new Vector2(-1, -1);
        Respawn();
    }

    
    void FixedUpdate()
    {
        Move();

        if (Input.GetKey(KeyBind.keys["Jump"]))
        {
            Jump();
        }
        InLadder();

        RaycastHit2D raycastRightG = Physics2D.Raycast(transform.position, rayRight, 0.5f, LayerMask.GetMask("Ground"));
        RaycastHit2D raycastRightW = Physics2D.Raycast(transform.position, rayRight, 0.5f, LayerMask.GetMask("Wall"));
        RaycastHit2D raycastLeftG = Physics2D.Raycast(transform.position, rayLeft, 0.5f, LayerMask.GetMask("Ground"));
        RaycastHit2D raycastLeftW = Physics2D.Raycast(transform.position, rayLeft, 0.5f, LayerMask.GetMask("Wall"));

        if (raycastRightG.collider != null || raycastLeftG.collider != null || raycastRightW.collider != null || raycastLeftW.collider != null)
        {
            groundCheck = true;
        }

        else
        {
            groundCheck = false;
        }

    }

    void Update()
    {
        // Debug.Log("Hp :" + Hp);
        

        if (Hp <= 0)
        {
            Revive();
        }
        SetMyHp();
    }

    void Move()
    {
        if (!inLadder)  // 사다리가 아닌 일반 움직임일 경우 원래대로
        {
            rigid2D.gravityScale = 1;   // 중력 1
            direction.y = 0;            // y축 입력받은 값 제거
        }

        direction.x = Input.GetAxisRaw("Horizontal"); // 좌우 입력받기, 좌=-1 우=1

        if (direction.x < 0) // 왼쪽이동
        {
            if (!isJumping)
            {
                animator.SetBool("isWalk", true);
            }
            transform.localScale = new Vector3(-1, 1, 1);
            //spriteRenderer.flipX = true; // 이미지 좌우 반전시킴 (기본 이미지가 오른쪽 바라보고 있을경우)
        }
        else if (direction.x > 0)
        {
            if (!isJumping)
            {
                animator.SetBool("isWalk", true);
            }
            transform.localScale = new Vector3(1, 1, 1);
            //spriteRenderer.flipX = false; // 이미지 원래대로
        }
        else if(direction.x == 0)
        {
            animator.SetBool("isWalk", false);

        }
        
        transform.position += direction * speed * Time.smoothDeltaTime; // 이동
    }

    public void Jump()  // Spring 스크립트에서 사용하려고 public으로 
    {
        if (isGround && (!inLadder))
        {
            rigid2D.velocity = Vector2.zero;
            rigid2D.AddForce(new Vector2(0, jumpPower), ForceMode2D.Impulse); // y축 힘을 가하여 점프
            isGround = false;
            isJumping = true;
            animator.SetBool("isJump", true);
            animator.SetBool("isWalk", false);

            if (!inLadder)
                playSound("Jump");
        }
    }

    void InLadder()     // 사다리 타는 함수
    {
        
        if (inLadder)   // 올라갈 수 있는 조건이면
        {
            rigid2D.velocity = Vector2.zero;    // 점프 방지
            rigid2D.gravityScale = 0;   // 미끄러지지 않게 중력 0으로 설정
            direction.y = Input.GetAxisRaw("Vertical"); // 위아래 입력만 받고 실제 이동은 Move에서 한번에
            animator.SetBool("isWalk", false);
            animator.SetBool("isJump", false);
            animator.SetBool("inLadder", true);
            // if(Input.GetKey(KeyBind.keys["Jump"])) {    // 사다리에서 점프 안되게 막음
            //     rigid2D.velocity = Vector2.zero;
            //     isGround = true;
            // }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        isJumping = false;
        if (collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "Obstacle" || collision.gameObject.tag == "Boss")
        {
            onDamaged(collision.transform.position);        // Enemy, Obstacle 충돌 시 위치를 매개변수로 넘김
        }

        if (collision.collider.tag == "Ground") // 땅이면
        {

            isGround = true; // 점프 가능
            isJumping = false;  // 착지
            animator.SetBool("isJump", false);
            animator.SetBool("inLadder", false);
        }
       
        if (collision.gameObject.tag == "Boss" && this.transform.position.x >= 230.5)
        {
            this.gameObject.SetActive(false);
        }
       
        if (collision.collider.tag == "SlowPoint") // 느린지점 이면
        {
            speed -= 2; //플레이어속도 slowspeed만큼 감소
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.tag == "Ground") // 땅에서 나오면
        {
            isGround = false; // 점프 비활성화
            animator.SetBool("isWalk", false);
        }

        if(collision.collider.tag == "SlowPoint") // 느린지점 에서 나오면
        {
            speed += 2; //플레이어속도 slowspeed만큼 증가
            isGround = true;
        }

    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        isJumping = false;
        if(col.tag == "WindZone") //바람 부는 지역 들어갈시 스크립트 활성화
        {
            gameObject.GetComponent<WindEffect>().enabled = true;
            Debug.Log("in");
        }

        if (col.tag == "Item")
        {
            playSound("Item");
        }

        if (col.tag == "JumpItem")
        {
            playSound("JumpItem");
        }

        if (col.tag == "CheckPoint")
        {
            playSound("CheckPoint");
        }

        if (col.tag == "Fallen")
        {
            playSound("Fallen");
        }

        if (col.tag == "Clear")
        {
            playSound("Clear");
        }
    }

    private void OnTriggerExit2D(Collider2D col) //바람 부는 지역 나올시 스크립트 비활성화
    {
        if (col.tag == "WindZone")
        {
            gameObject.GetComponent<WindEffect>().enabled = false;
            this.rigid2D.AddForce(Vector3.right * 1); //밀리는 힘들 정상화
            this.rigid2D.AddForce(Vector3.left * 1);
            Debug.Log("out");
        }
        
        if (col.tag == "Ladder")    // 사다리 벗어나면 애니메이션 해제
        {
            // animator.SetBool("isJump", true);
            animator.SetBool("inLadder", false);
        }

    }

    public void onDamaged(Vector2 targetPos)
    {
        playSound("Damaged");
        int dir = transform.position.x - targetPos.x > 0 ? 1 : -1;  // 피격시 튕겨나가는 방향 결정
        gameObject.layer = 9;                                       //플레이어의 Layer -> GodMode로 변경
        spriteRenderer.color = new Color(1, 1, 1, 0.3f);            // 투명하게 변경
        rigid2D.AddForce(new Vector2(dir, 1) * collisionForce, ForceMode2D.Impulse); // 튕겨나가기
        Invoke("offDamaged", invincibleTime);                       // invincibleTime 후 무적 시간 끝
    }

    void offDamaged()
    {
        gameObject.layer = 6;                                   // 플레이어의 Layer -> Player로 변경
        spriteRenderer.color = new Color(1, 1, 1, 1f);          // 색 원래대로 변경
    }

    void SetMyHp()
    {
        //Debug.Log();
       myHp.text = string.Format("X  {0}", Hp);    // 체력 표시
        
        // if (Hp < 1)
        // {
        //     Respawn();
        //     Hp = 100;
        // }

        // if (Hp > 100)
        // {
        //     Hp = 100;
        // }
    }

    public void playSound(string action)
    {
        switch (action) // 동작 별 사운드  
        {
            case "Jump": //점프
                audioSource.clip = audioJump;
                break;
            case "Damaged": //데미지 입음
                audioSource.clip = audioDamaged;
                break;
            case "Item": //아이템 획득
                audioSource.clip = audioItem;
                break;
            case "JumpItem": // 점프 아이템 획득
                audioSource.clip = audioJumpItem;
                break;
            case "CheckPoint": // 체크포인트 도달
                audioSource.clip = audioCheckPoint;
                break;
            case "Fallen": // 낙하
                audioSource.clip = audioFallen;
                break;
            case "Die": // 죽음
                audioSource.clip = audioDie;
                break;
            case "Clear": // 스테이지 클리어
                audioSource.clip = audioClear;
                break;
            case "Finish": //최종 클리어 
                audioSource.clip = audioFinish;
                break;
            case "Open_Hidden":
                audioSource.clip = audioHidden;
                break;
        }
        audioSource.Play();
    }

    public void Respawn()
    {
        this.transform.GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0, 0);
        this.transform.position = spawnPoint.transform.position;
    }

    public void Revive()
    {
        animator.SetBool("isWalk", false);
        animator.SetBool("isJump", false);
        animator.SetBool("inLadder", false);
        animator.SetBool("isDead", true);

        Hp = 5;
        isGround = true;
        savePlayer.save_hp = 5;
        savePlayer.GameSave();
        
        Invoke("Revive_Load", 1.2f);
    }

    private void Revive_Load()
    {
        int sceneNumber = SceneManager.GetActiveScene().buildIndex;
        Debug.Log(sceneNumber);

        animator.SetBool("isDead", false);
        if(sceneNumber == 6)
        {
            SceneManager.LoadScene(6);
        }
        else if(sceneNumber == 7)
        {
            SceneManager.LoadScene(7);
        }
        else if(sceneNumber >= 2 && sceneNumber <= 5)
        {
            SceneManager.LoadScene(2);
        }
        else if(sceneNumber >= 8 && sceneNumber <= 11)
        {
            SceneManager.LoadScene(8);
        }
        else if(sceneNumber >= 12 && sceneNumber <= 16)
        {
            SceneManager.LoadScene(12);
        }
    }
}
