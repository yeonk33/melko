using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BrokenEffect : MonoBehaviour
{
    Player player;
    Image image;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        image = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if(player.Hp < 1)
        {
            image.enabled = true;
        }else{
            image.enabled = false;
        }
    }
}
