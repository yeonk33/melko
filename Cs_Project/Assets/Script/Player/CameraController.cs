using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    // public GameObject player;
    public Player player;
    public float offsetX;
    public float offsetY;
    private Vector3 cameraOffset;
    public float speed;
    public Vector3 targetPosition;
    // Vector3 vel = new Vector3(10,10,0);

    // 박스 콜라이더 영역의 최대 최소값
    public BoxCollider2D bound;
    private Vector3 minBound;
    private Vector3 maxBound;

    // 카메라의 반너비, 반높이
    public float halfWidth;
    public float halfHeight;

    // 카메라의 반높이 값을 구하기 위한 속성 이용
    private Camera myCamera;

    private float time = 0;
    private bool camera_down = false;

    void Start()
    {
        cameraOffset = new Vector3(offsetX, offsetY, -10);
        myCamera = GetComponent<Camera>();
        minBound = bound.bounds.min;
        maxBound = bound.bounds.max;
        halfHeight = myCamera.orthographicSize;
        halfWidth = halfHeight * Screen.width / Screen.height;
        SetBound(bound);
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        // Application.targetFrameRate = 120;
    }

    void FixedUpdate()
    {
        if(player.isJumping) targetPosition.x = player.transform.position.x + cameraOffset.x;
        else targetPosition = player.transform.position + cameraOffset;

        if(player.speed > 10.0f) speed = 13f;
        else speed = 3f;
        
        // // 카메라의 중심보다 아래에 있으면 카메라 y좌표 고정
        // if (player.transform.position.y < transform.position.y)
        // {
        //     targetPosition.y = transform.position.y;
        // }
        // // 아래로 내려가면 카메라 y좌표는 플레이어 따라가도록
        // if (transform.position.y - player.transform.position.y > cameraOffset.y )
        // {
        //     targetPosition.y = player.transform.position.y + cameraOffset.y;
        // }

        // 아래키를 누르면
        if(Input.GetAxisRaw("Vertical") < 0)
        {
            camera_down = true;
            Debug.Log("Down");
        }

        if(camera_down)
        {
            time += Time.deltaTime;
        }

        // 아래키를 때면
        if(Input.GetAxisRaw("Vertical") == 0)
        {
            camera_down = false;
            time = 0;
        }

        // 아래키를 1초 이상으로 눌렀으면 카메라 이동
        if(time >= 1)
        {
            targetPosition.y = player.transform.position.y + cameraOffset.y - 5;
        }

        // Lerp함수 사용하여 카메라 부드럽게 이동
        transform.position = Vector3.Lerp(transform.position, targetPosition, Time.smoothDeltaTime * speed);
        // transform.position = Vector3.Lerp(transform.position, targetPosition, 0.025f); //Time.deltaTime * speed
        // transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref vel, 1f);


        //카메라의 위치가 min보다 작으면 min이고, max보다 크면 max 리턴 
        float clampedX = Mathf.Clamp(transform.position.x, minBound.x + halfWidth, maxBound.x - halfWidth);
        float clampedY = Mathf.Clamp(transform.position.y, minBound.y + halfHeight, maxBound.y - halfHeight);
        transform.position = new Vector3(clampedX, clampedY, transform.position.z);
    }
    
    private void SetBound(BoxCollider2D bound)
    {
        this.bound = bound;
        minBound = this.bound.bounds.min;
        maxBound = this.bound.bounds.max;
    }
}
