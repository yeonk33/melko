using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShakeEffect : MonoBehaviour
{
    public CameraController myCamera;
    public GameObject rock;
    public bool earthquake;
    Vector3 randomPos;
    // 카메라 흔들림 효과
    float timer = 0f;
    float rockTimer = 0f;
    [SerializeField] private float period;   // 흔들림 주기
    [SerializeField] public float duration;   // 흔들림 지속시간
    [SerializeField] private float magnitude;   // 흔들림 강도

    // Start is called before the first frame update
    void Start()
    {
        
    }


    void FixedUpdate()
    {
        if (earthquake)
        {
            rockTimer += Time.deltaTime;
            if (rockTimer > 0.3f)   // 0.3초마다 돌 생성
            {
                SpawnRock();
                rockTimer = 0;
            } 
        }

        timer += Time.deltaTime;
        if (timer > period) // 주기마다 흔들효과 발생
        {
            StartCoroutine(Shake(duration, magnitude));
            timer = 0f;
        }     
    }

    private IEnumerator Shake(float duration, float magnitude)   // 지속시간을 파라미터로
    {
        float elapsed = 0f; // 지속시간 체크할 변수
        earthquake = true;
        while (elapsed < duration)  // 지속시간동안 흔들림
        {
            float x = Random.Range(-1f, 1f) * magnitude;
            float y = Random.Range(-1f, 1f) * magnitude;
            transform.position = Vector3.Lerp(transform.position, myCamera.targetPosition, Time.deltaTime * myCamera.speed) +  new Vector3(x, y, 0);
            elapsed += Time.deltaTime;
            yield return null;
        }
        transform.position = Vector3.Lerp(transform.position, myCamera.targetPosition, Time.deltaTime * myCamera.speed);
        earthquake = false;
    }

    void SpawnRock()
    {
        randomPos = new Vector3(myCamera.transform.position.x + Random.Range(-myCamera.halfHeight, myCamera.halfHeight), myCamera.transform.position.y + myCamera.halfHeight, 0);
        Instantiate(rock, randomPos, Quaternion.identity);
    }
}
