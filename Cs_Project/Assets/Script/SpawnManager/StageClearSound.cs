using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageClearSound : MonoBehaviour
{
    BoxCollider2D boxCollider2D;
    Player player;


    // Start is called before the first frame update
    void Start()
    {
        boxCollider2D = GetComponent<BoxCollider2D>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            boxCollider2D.enabled = false; // 콜라이더 비활성화
        }
    }
}
