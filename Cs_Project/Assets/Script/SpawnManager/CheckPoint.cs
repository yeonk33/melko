using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckPoint : MonoBehaviour
{
    BoxCollider2D boxCollider2D;
    Player player;
    Vector2 spawnPoint = new Vector2();

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
            player.spawnPoint = this.transform;
            boxCollider2D.enabled = false;
        }
    }
}