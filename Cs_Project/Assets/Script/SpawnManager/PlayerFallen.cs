using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerFallen : MonoBehaviour
{
    Player player;
    [SerializeField] private int damage = 30;             // 데미지

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            Fall();
        }
    }

    private void Fall()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        player.Hp = (float)(Math.Truncate(player.Hp * 5)/10);                       // 체력 감소
        player.fallen = true;
        player.Respawn();                     // 플레이어 리스폰
        StartCoroutine(WaitForIt());
    }

    IEnumerator WaitForIt()
    {
        yield return new WaitForSeconds(0.5f);
        player.fallen = false;
    }
}
