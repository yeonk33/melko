using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage_Stage3 : MonoBehaviour
{
    private float tempColor;
    Player player;
    SpriteRenderer player_sprite;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        player_sprite = GameObject.FindGameObjectWithTag("Player").GetComponent<SpriteRenderer>();

        tempColor = 0;
        player.playerfired = 0; // 과열 0으로 시작.

        InvokeRepeating("Canfired", 1f, 1f);
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(player.Hp);
        //Debug.Log(player.playerfired);

        tempColor = player.playerfired * 0.01f;
        player_sprite.color = new Color(1, 1 - tempColor, 1 - tempColor , 1);
    }

    private void Canfired() // 과열함수
    {
        if(player.playerfired > 75) // 과열상태가 76부터
        {
            player.Hp -= 0.5f; // 체력 감소

            if(player.playerfired > 99) // 과열 상태가 100이면.
            {
                Debug.Log("Warning");
            }else{
                player.playerfired += 1; // 76부터 1씩 증가.
            }
        }else
        {
            player.playerfired += 2; // 0 ~ 75 까지 2씩 증가.
        }
    }
}
