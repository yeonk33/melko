using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2_Frozen : MonoBehaviour
{
    // 2스테이지에서 이 스크립트 플레이어에 붙이기

    public Player player;
    public bool isFrozen = false;
    //public bool isMoving = true;
    public float timerSet;
    public float timer;
    public float iceTime;
    Rigidbody2D rigid2D;
    public float maxSpeed;
    public bool onIce = false;

    // Start is called before the first frame update
    void Start()
    {
        timer = 0f;
        rigid2D = GetComponent<Rigidbody2D>();
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        timer += Time.deltaTime;

        if (player.direction.x != 0)    // 플레이어가 움직이면
        {
            timer = 0f; // 타이머 초기화

            // if (onIce)  // 빙판길이면
            // {
            //     player.speed = 1f;  // 원래 속도 줄임
            //     if (player.direction.x > 0) // 오른쪽으로 움직임
            //     {
            //         rigid2D.AddForce(new Vector2(2f, 0), ForceMode2D.Impulse); // 가속도로 미끄러지게
            //         if (rigid2D.velocity.x > maxSpeed)
            //         {
            //             rigid2D.velocity = new Vector2(maxSpeed, rigid2D.velocity.y);
            //             Debug.Log(rigid2D.velocity.x);
            //         }
            //      }
            //      else if (player.direction.x < 0) // 왼쪽으로 움직임
            //     {
            //         rigid2D.AddForce(new Vector2(-2f, 0), ForceMode2D.Impulse);
            //         if (rigid2D.velocity.x < -maxSpeed)
            //         {
            //             rigid2D.velocity = new Vector2(-maxSpeed, rigid2D.velocity.y);
            //         }
            //     }
            // }else
            // {
            //     player.speed = 3f; // 속도 원상복구
            // }
            
        }
        
        if (timer > timerSet)   // 설정한 시간동안 움직이지 않으면
        {
            StartCoroutine(Frozen());   // 조건 설정
            timer = -iceTime;   
        }
        if (isFrozen)
        {
            //Debug.Log("움직이지마");
            // 플레이어의 이동값을 빼줘서 총 움직이는 값이 0이 되어 안움직이게 함
            transform.position -= player.direction * player.speed * Time.deltaTime;
        }
    }

    IEnumerator Frozen()
    {
        //Debug.Log("얼음");
        isFrozen = true;    // 얼어붙고
        player.spriteRenderer.color = Color.blue;
        yield return new WaitForSeconds(iceTime);  // 지정한 시간 후에
        //Debug.Log("땡");       
        isFrozen = false;   // 얼음 땡
        player.Hp -= 0.5f;    // 데미지 입음
        player.spriteRenderer.color = Color.white;
    }
}
