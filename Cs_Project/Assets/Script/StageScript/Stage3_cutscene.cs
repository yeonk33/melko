using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage3_cutscene : MonoBehaviour
{
    public string[] sentences;
    public Transform chatTr;
    public GameObject chatBoxPrefab;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 0f;
        GameObject go = Instantiate(chatBoxPrefab);
        go.GetComponent<ChatSystem>().Ondialogue(sentences, chatTr);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyBind.keys["Interact"])){
            Time.timeScale = 1f;
            Destroy(GameObject.Find(chatBoxPrefab.name+"(Clone)"));
        }
    }
}
