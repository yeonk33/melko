using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Stage3_LoadRandomScene : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            if(Title.stage3_randomList.Count == 0 || Title.stage3_randomList == null)
            {
                SceneManager.LoadScene(16);
            }else
            {
                Title.stage3_MixScene();
                SceneManager.LoadScene(Title.stage3_randomList[0]);
                Title.stage3_randomList.RemoveAt(0);
            }
        }
    }
}
