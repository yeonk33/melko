using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
 
public class FirstClear : MonoBehaviour
{
    LoadManager savePlayer;
    public string sceneName = "GameTitle";
    public string scene_save = "Stage0_mineral";
 
    void start()
    {
        savePlayer = GameObject.Find("LoadManager").GetComponent<LoadManager>();
    }
 
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player" && this.transform.position.x >= 230)
        {
            Invoke("sceneLoad", 22.2f);
        }
    }
 
    public void sceneLoad()
    {
        PlayerPrefs.SetInt("End?", 1);
        PlayerPrefs.SetString("SceneName", scene_save);
        PlayerPrefs.SetFloat("MyHp", 5);
        /*
        savePlayer.save_Scene_Name = scene_save;
        savePlayer.save_hp = 5;
        savePlayer.save_end = 1;
        savePlayer.GameEnd();
        savePlayer.GameSave();
        */
        SceneManager.LoadScene(sceneName);
    }
}