using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Stage2_LoadRandomScene : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            if(Title.stage2_randomList.Count == 0 || Title.stage2_randomList == null)
            {
                SceneManager.LoadScene(11);
            }else
            {
                Title.stage1_MixScene();
                SceneManager.LoadScene(Title.stage2_randomList[0]);
                Title.stage2_randomList.RemoveAt(0);
            }
        }
    }
}
