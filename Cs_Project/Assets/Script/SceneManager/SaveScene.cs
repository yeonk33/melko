using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveScene : MonoBehaviour
{
    LoadManager savePlayer;
    Player player;

    public string sceneName = "PlayerTestMap";
    public int endgame = 0;
    // Start is called before the first frame update
    void Start()
    {
        savePlayer = GameObject.Find("LoadManager").GetComponent<LoadManager>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            savePlayer.save_Scene_Name = sceneName;
            savePlayer.save_hp = player.Hp;
            //savePlayer.save_end = endgame;
            savePlayer.GameSave();
        }
    }
}
