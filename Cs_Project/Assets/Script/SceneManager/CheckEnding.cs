using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CheckEnding : MonoBehaviour
{
    LoadManager savePlayer;
    public string sceneName = "GameTitle_second";

    void start()
    {
        savePlayer = GameObject.Find("LoadManager").GetComponent<LoadManager>();

        if (savePlayer.save_end == 1)
        {
            SceneManager.LoadScene(sceneName);
        }
    }
}