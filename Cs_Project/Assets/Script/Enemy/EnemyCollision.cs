using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyCollision : MonoBehaviour
{
    Player player;
    public SpriteRenderer playerRenderer;
    [SerializeField] private int damage = 10;             // 데미지
    [SerializeField] private float WaitTime = 2.0f;       // 충돌후 잠시 무적시간
    [SerializeField] private int layerGodMode = 9;        // 레이어 GodMode는 9번
    [SerializeField] private int layerPlayer = 6;         // 레이어 Player는 6번
    [SerializeField] private float reviveTime = 30.0f;    // 적 리스폰 시간
    public Slider HpBar;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player")
        {                                           
            if (transform.position.y < collision.transform.position.y - 0.3)      // Player의 y값이 enemy의 y값보다 크면
            {
                Die();                              // enemy 처치       
                Invoke("Revive", reviveTime);       // reviveTime 후 부활
                //StartCoroutine("Respawn");
            }
            else
                StartCoroutine("Collision");
        }
    }

    IEnumerator Collision()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        player.Hp -= damage;                            // 체력 감소
        yield return 0;
        /*
        player.gameObject.layer = layerGodMode;         // GodMode로 Enemey와 잠시 충돌 안하게 함 
        yield return new WaitForSeconds(WaitTime);      // 무적시간 지난 후
        player.gameObject.layer = layerPlayer;          // 다시 layer를 Player로 설정하여 정상 작동함
        */
    }

    /*
    IEnumerator Respawn()
    {
        gameObject.SetActive(false);                   // enemy 처치
        yield return new WaitForSeconds(reviveTime);   // 리스폰 시간 지난 후
        GameObject.Find("Enemy").transform.GetChild(0).gameObject.SetActive(true);    // 부활
    }
    */

    private void Die()
    {
        this.gameObject.SetActive(false); // 적 처치
        HpBar.gameObject.SetActive(false); // hp바 없어짐
    }

    private void Revive()
    {
        this.gameObject.SetActive(true); // 적 재생성
        HpBar.gameObject.SetActive(true); //hp바 재생성
    }
}
