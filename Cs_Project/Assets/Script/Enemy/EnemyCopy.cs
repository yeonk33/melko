using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyCopy : MonoBehaviour
{
    public Vector3 direction;
    public SpriteRenderer spriteRenderer;
    public Rigidbody2D rigid2D;

    [SerializeField] public float speed = 6.0f;
    [SerializeField] public float jumpPower = 5.0f;
    [SerializeField] public bool isGround = true;
    [SerializeField] public bool inLadder = false;
    [SerializeField] public int collisionForce = 3;
    [SerializeField] public float invincibleTime = 1.5f;
    [SerializeField] public int playerfired = 0;
    [SerializeField] public bool GetMine = false;
    [SerializeField] private Text myHp;

    public Animator animator;
    public Transform transform;
    ParachuteItem parachuteitem;
    ParachuteItem_ver2 parachuteitem2;


    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        rigid2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        transform = GetComponent<Transform>();
    }


    void FixedUpdate()
    {
        Move();
        if (Input.GetKey(KeyBind.keys["Jump"]))
        {
            Invoke("Jump", 1);
        }
        InLadder();

    }

    void Update()
    {

    }

    void Move()
    {
        if (!inLadder)  // 사다리가 아닌 일반 움직임일 경우 원래대로
        {
            rigid2D.gravityScale = 1;   // 중력 1
            direction.y = 0;            // y축 입력받은 값 제거
        }

        direction.x = Input.GetAxisRaw("Horizontal"); // 좌우 입력받기, 좌=-1 우=1

        if (direction.x < 0) // 왼쪽이동
        {
            animator.SetBool("isWalk", true);
            transform.localScale = new Vector3(-1, 1, 1);
            //spriteRenderer.flipX = true; // 이미지 좌우 반전시킴 (기본 이미지가 오른쪽 바라보고 있을경우)
        }
        else if (direction.x > 0)
        {
            animator.SetBool("isWalk", true);
            transform.localScale = new Vector3(1, 1, 1);
            //spriteRenderer.flipX = false; // 이미지 원래대로
        }
        else if (direction.x == 0)
        {
            animator.SetBool("isWalk", false);

        }

        Invoke("walk", 1);
    }

    public void walk()
    {
        transform.position += direction * speed * Time.deltaTime; // 이동
    }

    public void Jump()  // Spring 스크립트에서 사용하려고 public으로 
    {
        if (isGround)
        {
            rigid2D.velocity = Vector2.zero;
            rigid2D.AddForce(new Vector2(0, jumpPower), ForceMode2D.Impulse); // y축 힘을 가하여 점프
            isGround = false;
            animator.SetBool("isJump", true);
        }
    }

    void InLadder()     // 사다리 타는 함수
    {
        if (inLadder)   // 올라갈 수 있는 조건이면
        {
            rigid2D.gravityScale = 0;   // 미끄러지지 않게 중력 0으로 설정
            direction.y = Input.GetAxisRaw("Vertical"); // 위아래 입력만 받고 실제 이동은 Move에서 한번에
            Debug.Log(rigid2D.velocity);
            if (Input.GetKey(KeyCode.UpArrow) && Input.GetKey(KeyBind.keys["Jump"]))
            {
                rigid2D.velocity = Vector2.zero;    // 사다리에서 위로 점프 못하게 함
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Ground") // 땅이면
        {

            isGround = true; // 점프 가능
            animator.SetBool("isJump", false);
        }

        if (collision.collider.tag == "SlowPoint") // 느린지점 이면
        {
            speed -= 2; //플레이어속도 slowspeed만큼 감소
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.tag == "Ground") // 땅에서 나오면
        {
            isGround = false; // 점프 비활성화
        }

        if (collision.collider.tag == "SlowPoint") // 느린지점 에서 나오면
        {
            speed += 2; //플레이어속도 slowspeed만큼 증가
        }

    }

    private void OnTriggerEnter2D(Collider2D col) //바람 부는 지역 들어갈시 스크립트 활성화
    {
        if (col.tag == "WindZone")
        {
            gameObject.GetComponent<WindEffect>().enabled = true;
            Debug.Log("in");
        }
    }

    private void OnTriggerExit2D(Collider2D col) //바람 부는 지역 나올시 스크립트 비활성화
    {
        if (col.tag == "WindZone")
        {
            gameObject.GetComponent<WindEffect>().enabled = false;
            this.rigid2D.AddForce(Vector3.right * 1); //밀리는 힘들 정상화
            this.rigid2D.AddForce(Vector3.left * 1);
            Debug.Log("out");
        }
    }
}