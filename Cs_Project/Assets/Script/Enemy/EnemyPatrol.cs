using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : MonoBehaviour
{
    public float speed;
    public float jumpInterval;
    public float jumpStarttime;
    public float jumpPower;

    Rigidbody2D rigid;

    bool isRight = true;

    // Start is called before the first frame update
    void Start()
    {
        rigid = GetComponent<Rigidbody2D>();

        InvokeRepeating("Jump", jumpStarttime, jumpInterval);        // jumpIStarttime 초 후부터 jumpInterval 초 간격으로 점프
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Move();
    }

    private void Move()
    {
        transform.Translate(Vector2.right * speed * Time.deltaTime);    // 이동
    }

    private void Jump()
    {
        rigid.AddForce(Vector2.up * jumpPower, ForceMode2D.Impulse);    // 점프
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "endpoint")         // endpoint 태그가 달린 것과 충돌하면 방향 전환
        {
            if (isRight)
            {
                transform.eulerAngles = new Vector3(0, 180, 0);
                isRight = false;
            }
            else
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                isRight = true;
            }
        }
    }
}
