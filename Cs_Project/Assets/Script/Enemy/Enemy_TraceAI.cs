using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Enemy_TraceAI : MonoBehaviour
{
    public float speed;
    public float distance;
    public float jumpPower;
    Transform player;
    Rigidbody2D rig;
    public LayerMask groundLayer;
    

    void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player").transform; 
    }

    void Update()
    {   
        if(Mathf.Abs(transform.position.x - player.position.x) > distance) 
        { 
            transform.Translate(new Vector2(1, 0) * Time.deltaTime * speed);

            RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.right * 1f, 0.5f, groundLayer);
            RaycastHit2D hitdia = Physics2D.Raycast(transform.position, new Vector2(1 * DirectionPet(),1), 2f,groundLayer);
            if (player.position.y - transform.position.y <= 0)
            {
                hitdia = new RaycastHit2D();
            }

            if (hit || hitdia)
            {
                rig.velocity = Vector2.up * jumpPower;
            }
        }
       
    }

    float DirectionPet()
    {
        if(transform.position.x - player.position.x < 0)
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
            return -1;
        }
        else
        {
            transform.eulerAngles = new Vector3(0,180,0);
            return 1;
        }
    }
}
