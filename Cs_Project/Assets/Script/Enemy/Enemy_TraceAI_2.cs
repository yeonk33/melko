using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_TraceAI_2 : MonoBehaviour
{
    public float speed;
    public float distance;
    public float jumpPower;
    Transform player;
    Rigidbody2D rig;
    public LayerMask groundLayer;
    private float Dir;
    private float Height;


    void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        transform.Translate(new Vector2(1, 0) * Time.deltaTime * speed);
        Dir = Mathf.Abs(transform.position.x - player.position.x); //플레이어와 적의 x좌표 차이 절대값
        Height = Mathf.Abs(player.position.y - transform.position.y); //플레이어와 적의 y좌표 차이 절대값
        RaycastHit2D hitup = Physics2D.Raycast(transform.position, new Vector2(0, 1 * updown()), 2f, groundLayer); //플레이어와 높이에 따른 위, 아래로 레이캐스트
        //if (Mathf.Abs(transform.position.x - player.position.x) < distance)
        RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.right * 1f, 0.5f, groundLayer);//전방 레이캐스트
        RaycastHit2D hitdia = Physics2D.Raycast(transform.position, new Vector2(1 * DirectionPet(), 1), 5f, groundLayer);//적이 바라보는 방향 대각선 레이캐스트
        if (Height > 3 ) //적이 가까이 있고 높이가 다를때 
        {
            if (hitup) //땅이 위나 아래에 있다면
            {
                Escape();
            }
            if (hit)
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
            }
            if (hitdia && !hitup) //대가선이고 위에 땅이 없을때 점프
            {
                rig.velocity = Vector2.up * jumpPower;
            }
        }

        else //적이 멀리있거나 높이가 3이하 일때
        {
            Tracking();
        }
    }

    void Tracking()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.right * 1f, 0.5f, groundLayer);//전방 레이캐스트
        RaycastHit2D hitdia = Physics2D.Raycast(transform.position, new Vector2(1 * DirectionPet(), 1), 5f, groundLayer);//적이 바라보는 방향 대각선 레이캐스트
        RaycastHit2D hitup = Physics2D.Raycast(transform.position, new Vector2(0, 1), 2f, groundLayer); //위로 레이캐스트

        if (transform.position.x - player.position.x < 0) //오른쪽 이동
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
        }
        else //왼쪽 이동
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
        }

        if (player.position.y - transform.position.y <= 0)
        {
            hitdia = new RaycastHit2D();
        }

        if (hit) //전방  땅이 있을시 점프
        {
            rig.velocity = Vector2.up * jumpPower;
        }


    }

    void Escape()
    {
        if (transform.position.x - player.position.x < 0) //적이 플레이어보다 왼쪽에 있을 시 왼쪽으로 이동
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
        }
        else //적이 플레이어보다 왼쪽에 있을 시 오른쪽으로 이동
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
        }
    }

    float DirectionPet()
    {
        if (transform.position.x - player.position.x < 0) //플레이어가 왼쪽에
        {
            return 1;
        }
        else //플레이어가 오른쪽에
        {
            return -1;
        }
    }

    float updown()
    {
        if (transform.position.y - player.position.y < 0)  //플레이어가 위에
        {
            return 1;
        }
        else //플레이어가 아래에
        {
            return -1;
        }
    }
}
