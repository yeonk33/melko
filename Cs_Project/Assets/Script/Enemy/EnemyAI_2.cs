using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EnemyAI_2 : MonoBehaviour
{
    public float speed;
    public float jumpInterval;
    public float jumpStarttime;
    public float jumpPower;
    public Transform target;
    Rigidbody2D rigid;
    bool isRight = true;

    void Start()
    {
        rigid = GetComponent<Rigidbody2D>();
        target = GameObject.FindGameObjectWithTag("Player").transform;
        InvokeRepeating("Jump", jumpStarttime, jumpInterval);        // jumpIStarttime 초 후부터 jumpInterval 초 간격으로 점프
    }

    void Update()
    {
        float distance = Vector3.Distance(transform.position, target.position); // 플레이어 사이 거리

        Move();

        if (distance < 3.5) //플레이어 x좌표 와 거리가 5이하이면 따라가기 실행
        {
            Target_Move();
            Debug.Log("Target_Move()");
        }

        else
        {
            Debug.Log("Move()");
            return;
        }
    }

    private void Move()
    {
        transform.Translate(Vector2.right * speed * Time.deltaTime);    // 이동
    }

    private void Jump()
    {
        rigid.AddForce(Vector2.up * jumpPower, ForceMode2D.Impulse);    // 점프
    }

    private void Target_Move() // 플레이어 따라가기
    {
        float dir = target.position.x - transform.position.x;
        dir = (dir < 0) ? -1 : 1;

        if (dir == -1) // 플레이어가 좌측에 있음
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
            isRight = false;
            Debug.Log("좌");
        }
        else if (dir == 1) // 플레이어가 우측에 있음
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
            isRight = true;
            Debug.Log("우");
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "endpoint") // 해당 범위 유지
        {
            if (isRight) // 좌측으로 무빙
            {
                transform.eulerAngles = new Vector3(0, 180, 0);
                isRight = false;
            }
            else //우측으로 무빙
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                isRight = true;
            }
        }
    }


}
