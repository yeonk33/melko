using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Enemy_UI : MonoBehaviour
{
    public GameObject enemy; // 해당 enemy 지정
    public Transform enemy_hp; // 해당 enemy 지정
    public Slider HpBar; // 사용할 hpbar지정
    public float maxHp; // 전체 체력
    public float currentHp; // 현재 체력

    void Update()
    {

        transform.position = enemy_hp.transform.position+new Vector3(0,1,0); //해당 enemy 머리위 hpbar위치 조정
        HpBar.value = currentHp / maxHp; // (현재 체력 / 전체 체력)체력 바 조정
    }
}
