﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI_psh : MonoBehaviour
{
    NavMeshAgent nav;
    [SerializeField]Transform target; 
    
    void Start () {
        nav = GetComponent<NavMeshAgent>(); 
    }        
    // Update is called once per frame    
    void Update () {
        if (nav.destination != target.position)
        {
            nav.SetDestination (target.position);        
        }        
        else        
        {
            nav.SetDestination (transform.position);
        }    
    }
}
    