using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceGround : MonoBehaviour
{
    public Stage2_Frozen s2;

    private void OnCollisionEnter2D(Collision2D col) {
        if (col.collider.tag == "Player")
        {
            s2.onIce = true;
        }    
    }
    private void OnCollisionExit2D(Collision2D col) {
        if (col.collider.tag == "Player")
        {
            s2.onIce = false;
        }
    }
    
}
