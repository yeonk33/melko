using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ladder : MonoBehaviour
{
    Player player;
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
            player.inLadder = true;     // 사다리 오르고 내리기 활성화
            player.rigid2D.velocity = Vector2.zero;     // 점프해서 중간에 매달려도 미끄러지지 않게 하기 위해
            Debug.Log(player.rigid2D.velocity);
            player.isGround = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
            player.inLadder = false;    // 사다리 오르고 내리기 비활성화
        }
    }
}
