using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    public GameObject Freezing;
    //public Transform pos;
    public int maxCount = 20;
    private float offSetX = 1.5f;
    private float offSetY = -0.4f;
    private int direction = 0; // ����
    bool pushFreeze = false;
    public float cooltime; // ��Ÿ�� �ο�
    private float curtime; 
    FreezingItem freeze2;

    public AudioClip audioShoot;
    AudioSource audioSource;

    void Start()
    {
        freeze2 = GameObject.FindGameObjectWithTag("FreezingItem").GetComponent<FreezingItem>();
    }

    void OnEnable()
    {
        pushFreeze = false;
        curtime = 0;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow)) // ������
        {
            direction = 0;
            offSetX = 1.5f;
            offSetY = -0.4f;
        }
        else if (Input.GetKey(KeyCode.LeftArrow)) // ����
        {
            direction = 1;
            offSetX = -1.5f;
            offSetY = -0.4f;
        }
        else if (Input.GetKey(KeyCode.UpArrow)) // ����
        {
            direction = 2;
            offSetX = 0f;
            offSetY = 1.5f;
        }
        else if (Input.GetKey(KeyCode.DownArrow)) // �Ʒ���
        {
            direction = 3;
            offSetX = 0f;
            offSetY = -1.5f;
        }
        /*
        if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.W))  //�ϵ�
        {
            direction = 4;
            offSetX = 1.5f;
            offSetY = 1.5f;
        }
        if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.W)) //�ϼ�
        {
            direction = 5;
            offSetX = -1.5f;
            offSetY = 1.5f;
        }
        if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.S)) //����
        {
            direction = 6;
            offSetX = 1.5f;
            offSetY = -1.5f;
        }
        if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.S)) //����
        {
            direction = 7;
            offSetX = -1.5f;
            offSetY = -1.5f;
        }*/
        if (curtime <= 0)
        {
            if (Input.GetKey(KeyBind.keys["Interact"])) //zŰ �Է½� �Ѿ˳���
            {
                if (pushFreeze == false)
                {
                    pushFreeze = true;
                    Vector3 area = GetComponent<SpriteRenderer>().bounds.size;
                    Vector3 newPos = this.transform.position;
                    newPos.x += offSetX;
                    newPos.y += offSetY;
                    GameObject freeze = Instantiate(Freezing) as GameObject;
                    newPos.z = -5;
                    freeze.transform.position = newPos;
                    Rigidbody2D rbody = freeze.GetComponent<Rigidbody2D>();
                    if (direction == 0)
                    {
                        rbody.AddForce(Vector2.right * 20, ForceMode2D.Impulse);
                        freeze.transform.eulerAngles = new Vector3(0, 0, -90);
                    }
                    else if (direction == 1)
                    {
                        rbody.AddForce(Vector2.left * 20, ForceMode2D.Impulse);
                        freeze.transform.eulerAngles = new Vector3(0, 0, 90);
                    }
                    else if (direction == 2)
                    {
                        rbody.AddForce(Vector2.up * 10, ForceMode2D.Impulse);
                        freeze.transform.eulerAngles = new Vector3(0, 0, 0);
                    }
                    else if (direction == 3)
                    {
                        rbody.AddForce(Vector2.down * 10, ForceMode2D.Impulse);
                        freeze.transform.eulerAngles = new Vector3(0, 0, 180);
                    }
                    /*
                    else if (direction == 4)
                        rbody.AddForce(new Vector2(10, 10), ForceMode2D.Impulse);
                    else if (direction == 5)
                        rbody.AddForce(new Vector2(-10, 10), ForceMode2D.Impulse);
                    else if (direction == 6)
                        rbody.AddForce(new Vector2(10, -10), ForceMode2D.Impulse);
                    else if (direction == 7)
                        rbody.AddForce(new Vector2(-10, -10), ForceMode2D.Impulse);*/
                }
                else
                {
                    pushFreeze = false;
                }
                freeze2.GunDisabled();
            }
            curtime = cooltime;
        }
        curtime -= Time.deltaTime;
        /*if (curtime <= 0)
        {
            if (Input.GetKey(KeyCode.Z))
            {
                Instantiate(Freezing, pos.position, transform.rotation);
            }
            curtime = cooltime;
        }
        curtime -= Time.deltaTime;*/
    }
    void playSound(string action)
    {
        switch (action)
        {
            case "Shoot":
                audioSource.clip = audioShoot;
                break;
        }
        audioSource.Play();
    }
}
