using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HiddenDoor : MonoBehaviour
{
    public GameObject door;

    void Start()
    {
        door.SetActive(false);
    }
}
