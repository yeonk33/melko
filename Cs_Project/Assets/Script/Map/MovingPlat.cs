using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlat : MonoBehaviour
{
    Vector3 startPosition;
    Vector3 curruntPosition;
    Vector3 targetPosition;

    [SerializeField] private float moveX = 0f;  // x축으로 몇칸
    [SerializeField] private float moveY = 0f;  // y축으로 몇칸
    [SerializeField] private float speed = 0.1f;  // 발판 속도
    //[SerializeField] private float speedFactor = 0.5f;  // 플레이어가 발판 위에 있도록 조절하기 위한 속도계수
    [SerializeField] private bool moveWithFlatform = false;
    [SerializeField] private float offsetX;
    [SerializeField] private float offsetY;
    [SerializeField] private Vector3 playerTargetPosition;

    Player player;

    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;     // 시작 위치
        curruntPosition = transform.position;   // 현재 위치
        targetPosition = new Vector3(curruntPosition.x + moveX, curruntPosition.y + moveY, 0);  // 목표지점
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        speed = 0.05f;
    }

    void FixedUpdate()
    {
        curruntPosition = transform.position;   // 현재 위치 계속 업데이트 
        transform.position = Vector3.MoveTowards(curruntPosition, targetPosition, speed); // 목표지점까지 이동 Time.deltaTime * speed
        
        if (moveWithFlatform)   // 플레이어 올라타면
        {
            // 발판과 플레이어의 거리, 높이 차이
            offsetX = player.transform.position.x - transform.position.x;
            offsetY = player.transform.position.y - transform.position.y;
            // 플레이어의 목적지는 발판위치에 플레이어와 발판중심의 거리, 높이 차이를 더한 위치
            playerTargetPosition = new Vector3(targetPosition.x + offsetX, targetPosition.y + offsetY, 0);
            // 발판과 같은 속도록 이동
            player.transform.position = Vector3.MoveTowards(player.transform.position, playerTargetPosition, speed); //Time.deltaTime * 
        }
        


        if (curruntPosition == targetPosition)  // 목표 지점에 도달하면
        {
            moveX = -moveX;     // x축 방향 전환
            moveY = -moveY;     // y축 방향 전환
            targetPosition = new Vector3(curruntPosition.x + moveX, curruntPosition.y + moveY, 0);  // 목표지점 재설정
            //targetPosition = new Vector3(startPosition.x, startPosition.y, 0);  // 목표지점을 시작위치로 변경
        }
        // if (curruntPosition == startPosition)   // 시작 위치까지 다시 되돌아가면
        // {
        //     moveX = -moveX;     // x축 방향 전환
        //     moveY = -moveY;     // y축 방향 전환
        //     targetPosition = new Vector3(curruntPosition.x + moveX, curruntPosition.y + moveY, 0);  // 목표지점 재설정
        //     //targetPosition = new Vector3(curruntPosition.x + moveX, curruntPosition.y + moveY, 0);  // 목표지점 다시 원래 목표지점으로
        // }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player")
        {
            moveWithFlatform = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player")
        {
            moveWithFlatform = true;
        }       
    }
}
