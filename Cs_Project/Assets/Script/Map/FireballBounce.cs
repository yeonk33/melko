using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballBounce : Obstacle
{
    [SerializeField] private float xforce;
    [SerializeField] private float yforce;
    [SerializeField] private float time;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("fireBounce"))
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(xforce, yforce), ForceMode2D.Impulse);
            Invoke("flipX", time);
        }
    }

    private void flipX()
    {
        this.transform.eulerAngles = new Vector3(0, 0, 180);
    }
}
