using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lava : MonoBehaviour
{
    Player player;
    public GameObject FreezingBlock; // 얼려진 블록
    public GameObject LavaBlock; // 용암블록

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    void Inlava()
    {
        player.Hp -= 0.5f;
        player.onDamaged(player.transform.position);
    }

    void LavaActive()
    {
        LavaBlock.gameObject.SetActive(true);// 용암 블록 비활성화
    }

    void FreezeActive()
    {
        FreezingBlock.gameObject.SetActive(true);// 얼려진 블록 활성화
    }

    void LavaDisabled()
    { 
        LavaBlock.gameObject.SetActive(false); // 용암 블록 활성화
    }

    void FreezeDisabled()
    {
        FreezingBlock.gameObject.SetActive(false); // 얼려진 블록 비활성화   
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            Debug.Log("용암");
            //Inlava();
            InvokeRepeating("Inlava", 0.1f, 2.0f);
        }

        if (collision.tag == "Bullet") {  // 얼음총 적중시 용암부분 얼려진 장판생성
            LavaDisabled();                 // 용암 비활성화 
            FreezeActive();                 // 굳은 블록 활성화
            Invoke("LavaActive", 4.0f);     // 4초 뒤 용암 활성화 
            Invoke("FreezeDisabled", 5.0f); // 5초 뒤 굳은 블록 비활성화 (1초 차이는 용암 이펙터의 로딩 시간)
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            CancelInvoke("Inlava");
        }
    }

}
