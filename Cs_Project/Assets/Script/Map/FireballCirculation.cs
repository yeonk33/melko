using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballCirculation : MonoBehaviour
{
    [SerializeField] private float radX;
    [SerializeField] private float radY;
    [SerializeField] private bool clockwise;
    [SerializeField] private float power;
    Vector2 pos;
    private bool isInvisible = false;


    // Start is called before the first frame update
    void Start()
    {
        pos = new Vector2(this.transform.position.x, this.transform.position.y);
        StartCoroutine(MovingSinCos(clockwise, radX, radY, power));
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.tag == "Ground")
        {
            if (isInvisible == false)
            {
                isInvisible = true;
                GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.1f);    // 땅에 닿고 나올 때 투명하게 (0.1f -> 0으로하면 완전투명)
            }

            else
            {
                isInvisible = false;
                GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
            }
        }
    }

    public IEnumerator MovingSinCos(bool clockwise, float radX, float radY, float power)
    {
        if (clockwise)
        {
            while (true)
            {
                for (int th = 0; th < 360; th++)
                {
                    var rad = Mathf.Deg2Rad * th;
                    var x = radX * Mathf.Sin(rad);
                    var y = radY * Mathf.Cos(rad);
                    this.transform.position = new Vector2(pos.x + radX + x, pos.y + y);
                    yield return new WaitForSeconds(power);
                }
            }
        }
        else
        {
            while (true)
            {
                for (int th = 360; th > 0; th--)
                {
                    var rad = Mathf.Deg2Rad * th;
                    var x = radX * Mathf.Sin(rad);
                    var y = radY * Mathf.Cos(rad);
                    this.transform.position = new Vector2(pos.x - radX + x, pos.y + y);
                    yield return new WaitForSeconds(power);
                }
            }
        }
    }
}