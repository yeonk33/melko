using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spring : MonoBehaviour
{
    Player player;
    [SerializeField] private float addPower = 5f;
    private void OnCollisionEnter2D(Collision2D collision)  // 스프링 밟으면
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
            player.isGround = true;         // 점프 하기 위해서 isGround true로 설정
            player.jumpPower += addPower;   // 스프링 밟으면 더 높은 점프
            player.Jump();                  // 점프
            player.jumpPower -= addPower;   // 점프 후에는 다시 원래 점프력으로
        }
    }
}