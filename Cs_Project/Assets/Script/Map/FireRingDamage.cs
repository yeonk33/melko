using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireRingDamage : MonoBehaviour
{
    [SerializeField] Player player;
    [SerializeField] int damage = 1;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            player.Hp -= damage;
        }
    }
}
