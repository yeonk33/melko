using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    [SerializeField] private GameObject FireBall;
    [SerializeField] private float delayTime;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(CreateFireballRoutine());
    }

    // Update is called once per frame
    void FixedUpdate()
    {

    }

    IEnumerator CreateFireballRoutine()
    {
        while(true)
        {
            CreateFireball();
            yield return new WaitForSeconds(delayTime);         // delayTime 후 재생성
        }
    }

    private void CreateFireball()
    {
        Vector2 pos = new Vector2(this.transform.position.x, this.transform.position.y);                     // 생성 위치
        Instantiate(FireBall, pos, Quaternion.identity);        // 오브젝트 복사
    }


}