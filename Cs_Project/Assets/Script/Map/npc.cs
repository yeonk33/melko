using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class npc : MonoBehaviour
{
    public string[] sentences;
    public string[] Clear_sentences;
    public Transform chatTr;
    public GameObject chatBoxPrefab;
    Player player;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }


    private void Update()
    {
        /*float distance = Vector3.Distance(transform.position, target.position); // 플레이어 사이 거리
    
        if (distance < 3.5) //플레이어 x좌표 와 거리가 3.5미만이면 chat 실행
        {
            TalkNpc();
            //StartCoroutine(WaitForIt());

            //Debug.Log("Talk");
        }

        else
        {
            return;
        }*/
  
    }

    public void TalkNpc() // chat띄우기
    {
        GameObject go = Instantiate(chatBoxPrefab);
        go.GetComponent<ChatSystem>().Ondialogue(sentences, chatTr);
        //Invoke("TalkNpc", 10f);
    }

    /*
    public void TalkNpc_Clear() // 클리어 한 뒤 다른 chat띄우기
    {
        GameObject go = Instantiate(chatBoxPrefab);
        go.GetComponent<ChatSystem>().Ondialogue(Clear_sentences, chatTr);
    }*/

    /*
    IEnumerator WaitForIt()
    {
        yield return new WaitForseconds(2.0f);
        TalkNpc();
    }*/
    /*
    private void OnTriggerEnter2D(Collider2D col) 
    {
        if (col.tag == "Player")
        {
            TalkNpc();
        }
    }*/

    private void OnTriggerEnter2D(Collider2D col) //npc 대화
    {
        /*if (savePlayer.save_end == 0)
        {
            if (col.tag == "Player")
            {
                TalkNpc_Clear();
            }
        }*/

        /*else
        {*/
        if (col.tag == "Player")
        {
            TalkNpc();
        }
        //}
    }

    private void OnTriggerExit2D(Collider2D col)//챗 삭제
    {
        if (col.tag == "Player")
        {
            Destroy(GameObject.Find(chatBoxPrefab.name+"(Clone)"));
        }
    }
}
