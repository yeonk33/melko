using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireSpout : MonoBehaviour
{
    [SerializeField] private float radY;
    [SerializeField] private float power;

    Vector2 pos;

    // Start is called before the first frame update
    void Start()
    {
        pos = new Vector2(this.transform.position.x, this.transform.position.y);
        StartCoroutine(MovingUpDown(radY,power));
    }

    // Update is called once per frame
    void Update()
    {

    }

    public IEnumerator MovingUpDown(float radY, float power)
    {
        while (true)
        {
            for (int th = 0; th < 360; th++)
            {
                    var rad = Mathf.Deg2Rad * th;
                    var y = radY * Mathf.Cos(rad);
                    this.transform.position = new Vector2(pos.x, pos.y + y);
                    yield return new WaitForSeconds(power);
            }
        }
    }
}