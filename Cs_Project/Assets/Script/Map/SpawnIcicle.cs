using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnIcicle : MonoBehaviour
{
    [SerializeField] private GameObject ICICLE;
    [SerializeField] private float delayTime = 1;
    public Icicle script; 

    void Awake()
    {
        script = ICICLE.GetComponent<Icicle>();
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(CreateIcicleRoutine());
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (script.isDestroy == true)     
        {
            CreateIcicle();
        }
    }

    IEnumerator CreateIcicleRoutine()
    {
        CreateIcicle();
        yield return new WaitForSeconds(delayTime);         // delayTime 후 재생성
        
    }

    private void CreateIcicle()
    {
        Vector2 pos = new Vector2(this.transform.position.x, this.transform.position.y);                     // 생성 위치
        Instantiate(ICICLE, pos, Quaternion.Euler(0, 0, 180.0f));        // 오브젝트 복사
    }


}