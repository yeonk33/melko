using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ending_2th : MonoBehaviour
{
    public GameObject go;
    public GameObject BackGround;
    void Awake()
    {
        BackGround.SetActive(true);
    }
    void SeceneMove()
    {
        SceneManager.LoadScene("GameTitle");
        //BackGround.SetActive(true);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            BackGround.SetActive(false);
            go.SetActive(true);
            Invoke("SeceneMove",16f);
        }
    }

}
