using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.SceneManagement;

public class CutScene_Destroy : MonoBehaviour
{
    //SpriteRenderer spriteRenderer;
    Tilemap tilemap;
    TilemapRenderer tilemapRenderer;
    //Player player;
    [SerializeField] private float currTime = 3; // 발판 재생성 시간 설정

    private float timer = 0;
    private bool activePlatItem = true; //아이템 활성상태
    private float platTime; // 흐른시간 저장 변수

    public string sceneName = "PlayerTestMap";
    public GameObject SoundManager;
    public string bgmName = "";

    // Start is called before the first frame update
    void Start()
    {
        //spriteRenderer = GetComponent<SpriteRenderer>();
        tilemap = GetComponent<Tilemap>();
        tilemapRenderer = GetComponent<TilemapRenderer>();
        SoundManager = GameObject.FindGameObjectWithTag("BgmManager");
        //player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        platTime = 0;
        Debug.Log(activePlatItem);
    }
    // Update is called once per frame
    void Update()
    {

        if(!activePlatItem) // 아이템이 비활성화 되어있으면
        {
            if(platTime < 0.5f) // 재생성시간이 흘렀다면
            {
                //spriteRenderer.color = new Color(1, 1, 1, 1 - platTime);
                tilemap.color = new Color(1, 1, 1, 1 - platTime);
            }else{
                //spriteRenderer.color = new Color(1, 1, 1, platTime);
                tilemap.color = new Color(1, 1, 1, platTime);

                if(platTime > 1f)
                {
                    platTime = 0;
                    timer += 1;
                }
            }

            if(timer == currTime)
            {
                timer = 0;
                tilemapRenderer.enabled = false; // 렌더러 비활성화
                //GetComponent<BoxCollider2D>().enabled = false; // 콜라이더 비활성화
                GetComponent<TilemapCollider2D>().enabled = false; // 콜라이더 비활성화
                activePlatItem = true; // 아이템 활성화
            }

            platTime += Time.deltaTime;
            Invoke("load", 7);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Player") && activePlatItem) // 플레이어이고, 아이템이 활성화되어있으면
        {
            Debug.Log(activePlatItem);
            Invoke("itemActive", 0.7f);
        }
    }

    private void Colliderenable()
    {
        //GetComponent<BoxCollider2D>().enabled = true;
        GetComponent<TilemapCollider2D>().enabled = true;
        tilemapRenderer.enabled = true;
    }

    private void itemActive()
    {
        activePlatItem = false;
    }

    private void load()
    {
        SoundManager.GetComponent<PlayMusicOperator>().PlayBGM(bgmName);
        SceneManager.LoadScene(sceneName);
    }
}