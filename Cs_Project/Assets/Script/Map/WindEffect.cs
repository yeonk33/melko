using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindEffect : MonoBehaviour
{
    Player player;
    public int WindDir = 0;
    public int speed = 10;
    public float maxspeed = 1.5f;

    void OnDisable()
    {
        WindDir = 0; //함수 비활성화시 바람 방향 0으로 설정
        player.speed = 3f;
    }

    void OnEnable()
    {
        Invoke("WindDirection", 3.0f); // 5초뒤 바람 방향 설정
        //StartCoroutine(RandomWindDirection());
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        if (WindDir == 1) //우측 바람
        {
            player.rigid2D.AddForce(Vector3.right * speed, ForceMode2D.Force);
            if (player.rigid2D.velocity.x >= maxspeed)
            {
                player.rigid2D.velocity = new Vector2(maxspeed, player.rigid2D.velocity.y);
            }
        }

        else if (WindDir == -1) // 좌측 바람
        {
            player.rigid2D.AddForce(Vector3.left * speed, ForceMode2D.Force);
            if (player.rigid2D.velocity.x <= -maxspeed)
            {
                player.rigid2D.velocity = new Vector2(-maxspeed, player.rigid2D.velocity.y);
            }
        }

        else if (WindDir == 0) // 바람x
        {
            player.rigid2D.AddForce(Vector3.right * 1, ForceMode2D.Force);
            player.rigid2D.AddForce(Vector3.left * 1, ForceMode2D.Force);
        }
    }

    void WindDirection()  // 바람 방향 설정
    {
        WindDir = Random.Range(-1, 2);
    }

    /*IEnumerator RandomWindDirection()
    {
        WindDirection();
        //resetPS();
        yield return new WaitForSecondsRealtime(5.0f);
        Debug.Log(WindDir);
    }*/
    /*
    void resetPS()
    {
        player.rigid2D.AddForce(Vector3.right * 1);
    }
    */

    /*
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            Debug.Log("in");
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            WindDir = 0;
        }
    }*/
}
