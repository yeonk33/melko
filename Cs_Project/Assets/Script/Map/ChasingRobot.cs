using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChasingRobot : MonoBehaviour
{
    public Player player;
    float speed;
    Rigidbody2D rigid2D;
    float jumpPower;
    Vector2 ray;
    Vector2 target;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        speed = player.speed + 0.1f;
        rigid2D = GetComponent<Rigidbody2D>();
        jumpPower = player.jumpPower;
        ray = new Vector2(1,-1);
    }

    void FixedUpdate()
    {
        // 플레이어 낙사시 리스폰지점으로 스폰
        if(player.fallen) transform.position = new Vector2(player.transform.position.x - 5, player.transform.position.y);


        target = new Vector2(player.transform.position.x, transform.position.y);
        // 플레이어 따라감
        transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime * speed);


        // 레이저 그리기
        Debug.DrawRay(transform.position, ray, Color.blue);
        //Debug.DrawRay(transform.position, ray, Color.green);
        // 오른쪽 턱 감지하는 레이캐스트
        RaycastHit2D raycastRight = Physics2D.Raycast(transform.position, ray, 0.8f, LayerMask.GetMask("Ground"));
        // 바닥이 있는지 감지하는 레이캐스트
        RaycastHit2D raycastDown = Physics2D.Raycast(transform.position, ray, 40f, LayerMask.GetMask("Ground"));
        RaycastHit2D raycastWall = Physics2D.Raycast(transform.position, ray, 2f, LayerMask.GetMask("Wall"));

        if(raycastRight.collider != null){  // 앞에 턱 있으면 점프
            Jump();
        }

        if((raycastDown.collider == null) || (raycastWall.collider != null)){   // 바닥 없으면 점프
            Jump();
        }
    }

    void Jump(){
        rigid2D.velocity = Vector2.zero;
        rigid2D.AddForce(new Vector2(0, jumpPower), ForceMode2D.Impulse); // y축 힘을 가하여 점프
    }
}
