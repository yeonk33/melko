using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundFallen : MonoBehaviour
{
    SpriteRenderer spriteRenderer;
    Player player;
    Vector3 pos;
    private bool notPlayer = true;
    [SerializeField] private float falling = 5; // 올라오는 속도
    [SerializeField] private float upSpeed = 1.0f; // 올라오는 속도
    [SerializeField] private float fallenSpeed = 2.0f; // 가라앉는 속도
    private float offsetY;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

        pos = this.gameObject.transform.position;
    }
    // Update is called once per frame
    void Update()
    {
        if(notPlayer)
        {
            transform.position = Vector3.MoveTowards(transform.position, pos, Time.deltaTime * upSpeed);
        }else{
            offsetY = player.transform.position.y - transform.position.y; // 발판과 플레이어 높이차이
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(pos.x, pos.y - falling, pos.z), Time.deltaTime * fallenSpeed);
            player.transform.position = Vector3.MoveTowards(player.transform.position, new Vector3(player.transform.position.x, transform.position.y + offsetY, 0), fallenSpeed);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) 
    {
        if(collision.gameObject.CompareTag("Player")) // 플레이어이면
        {
            notPlayer = false;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Player")) // 플레이어이면
        {
            notPlayer = true;
        }
    }
}
