using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snowball : MonoBehaviour
{
    Player player;
    Rigidbody2D rb;
    public GameObject Area;
    [SerializeField] private float xforce;
    [SerializeField] private float yforce;
    [SerializeField] float DestroyTime;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            rb.bodyType = RigidbodyType2D.Dynamic;
            GetComponent<Rigidbody2D>().AddForce(new Vector2(xforce, yforce), ForceMode2D.Impulse);
            Destroy(this.gameObject, DestroyTime); // 플레이어와 충돌하지 않으면 DestroyTime 초 후에 파괴
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            player.Hp -= 1;
            Destroy(this.gameObject);   // 플레이어와 충돌하면 바로 파괴
        }
    }
}
