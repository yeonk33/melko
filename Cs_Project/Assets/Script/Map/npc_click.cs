using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class npc_click : MonoBehaviour
{
    public string[] sentences;
    public string[] Clear_sentences;
    public Transform chatTr;
    public GameObject chatBoxPrefab;
    public bool col = false;
    Player player;
    LoadManager savePlayer;
    public GameObject Hiddenportal;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        savePlayer = GameObject.Find("LoadManager").GetComponent<LoadManager>();
    }

    void Update()
    {
        if (GameObject.Find(chatBoxPrefab.name + "(Clone)") == null && col == true)
        {
            if (Input.GetKeyDown(KeyBind.keys["Interact"]))
             {
                if (savePlayer.save_end == 1)
                {
                    TalkNpc_Clear();
                    if(SceneManager.GetActiveScene().name == "Stage2_LSH")
                    {
                        Hiddenportal.gameObject.SetActive(true);
                    }
                }
                else
                {
                    TalkNpc_Click();
                }
            }

        }
    }

    public void TalkNpc_Click() // chat����
    {
        GameObject go = Instantiate(chatBoxPrefab);
        go.GetComponent<ChatSystem_Click>().Ondialogue(sentences, chatTr);
    }

    public void TalkNpc_Clear() // chat����
    {
        GameObject go = Instantiate(chatBoxPrefab);
        go.GetComponent<ChatSystem_Click>().Ondialogue(Clear_sentences, chatTr);
    }

    private void OnTriggerEnter2D(Collider2D collision) //npc ��ȭ
    {
        if (collision.tag == "Player")
        {
            col = true;
        }
    }

 

    private void OnTriggerExit2D(Collider2D collision)//ê ����
    {
        if (collision.tag == "Player")
        {
            col = false;
            Destroy(GameObject.Find(chatBoxPrefab.name + "(Clone)"));
        }
    }
}
