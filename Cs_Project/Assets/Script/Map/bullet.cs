using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour
{
    Player player;
    Vector3 dir;
    CircleCollider2D col;
    [SerializeField] private float speed = 1;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();      
        dir = (player.transform.position - transform.position).normalized;
        col = GetComponent<CircleCollider2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.position += dir * speed * Time.deltaTime;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            // player.Hp -= 1;
            Destroy(this.gameObject);
        }
        else {
            col.isTrigger = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        col.isTrigger = false;
    }
}
