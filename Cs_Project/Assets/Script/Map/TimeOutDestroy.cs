using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeOutDestroy : MonoBehaviour
{
    [SerializeField] GameObject gmObject;   // 삭제할 오브젝트
    [SerializeField] float time;    // 몇초 뒤에 삭제할것인지
    void Start()
    {
        Destroy(gmObject, time);
    }
}
