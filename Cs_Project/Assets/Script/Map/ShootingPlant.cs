using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingPlant : MonoBehaviour
{
    public GameObject area;
    public GameObject bullet;
    SpriteRenderer sr;
    Player player;
    [SerializeField] private bool isShoot = false;
    private float timer;
    [SerializeField] private float shootTime;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        sr = GetComponent<SpriteRenderer>();
    }

    private void FixedUpdate()
    {
        if (isShoot)
        {
            timer += Time.deltaTime;
            if (timer > shootTime)   // 정한 시간마다 한 발씩 발사
            {
                Instantiate(bullet, transform.position, Quaternion.identity);
                timer = 0;
            }
            
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            // 나중에 공격모션 에셋 적용
            sr.color = Color.red;
            isShoot = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            // 나중에 에셋 적용
            sr.color = Color.white;
            isShoot = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            player.Hp -= 1;
        }
    }
}
