using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireRingHeal : MonoBehaviour
{
    [SerializeField] private Player player;
    [SerializeField] private int heal;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
            if(player.Hp > 4)
                player.Hp = 5;
            else
                player.Hp += 1;
        }          
    }
}
