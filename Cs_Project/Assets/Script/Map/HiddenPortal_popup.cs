using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HiddenPortal_popup : MonoBehaviour
{
    public GameObject Hiddenportal;
    LoadManager savePlayer;
    
    void Start()
    {
        savePlayer = GameObject.Find("LoadManager").GetComponent<LoadManager>();
    }

    void HiddenportalActive()
    {
        Hiddenportal.gameObject.SetActive(true);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player" && savePlayer.save_end == 1)
        {
            if (Input.GetKeyDown(KeyBind.keys["Interact"]))
            {
                HiddenportalActive();
            }
        }
    }

}
